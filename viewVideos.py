from orig_vid import origVid
from procVid import procVid

from PyQt5.QtCore import QDir, Qt, QUrl
# from PyQt5.QtMultimedia import QMediaContent, QMediaPlayer
# from PyQt5.QtMultimediaWidgets import QVideoWidget
from PyQt5.QtWidgets import (QApplication, QFileDialog, QHBoxLayout, QLabel,
        QPushButton, QSizePolicy, QSlider, QStyle, QVBoxLayout, QWidget)
from PyQt5.QtWidgets import QMainWindow,QWidget, QPushButton, QAction
from PyQt5.QtGui import QIcon
from wildlife_detector_window import Ui_wildLifeDetector
import sys


class viewVid:
    def __init__(self,mainWindow):

        self.ui = mainWindow.ui
        self.viewTab = self.ui.tabWidget
        # self.originalVideo = origVid(self)
        self.processedVideo = procVid(self)

        # # File selection button
        # self.ui.InputFileButton.clicked.connect(self.onInputFileButtonClicked)
        # self.ui.outputFileButton.clicked.connect(self.onOutputFileButtonClicked)

        # self.ui.InputFileLineEdit.textChanged.connect(self.inputFileTextChanged)
        # self.ui.outputFileLineEdit.textChanged.connect(self.outputFileTextChanged)

    def onInputFileButtonClicked(self):
       filename, filter = QFileDialog.getOpenFileName(self.viewTab, "Open File","./", "Videos (*.mp4 *.avi)");

       if filename:
           self.ui.InputFileLineEdit.setText(filename)

#           # Load in a file to play
#           file = QDir.current().filePath(filename)
#           self.originalVideo.videoPlayerOrig.setMedia(QMediaContent(QUrl.fromLocalFile(file)))
#           self.originalVideo.videoPlayerOrig.setVideoOutput(self.ui.vidWidgetOrig)

    def onOutputFileButtonClicked(self):
       filename, filter = QFileDialog.getOpenFileName(self.viewTab, "Open File","./", "Images (*.mp4 *.avi)");

       if filename:
           self.ui.outputFileLineEdit.setText(filename)



    def inputFileTextChanged(self):

        input = self.ui.InputFileLineEdit.text()

        # # Load in a file to play
        # file = QDir.current().filePath(input)
        # self.originalVideo.videoPlayerOrig.setMedia(QMediaContent(QUrl.fromLocalFile(file)))
        # self.originalVideo.videoPlayerOrig.setVideoOutput(self.ui.vidWidgetOrig)

    def outputFileTextChanged(self):

        output = self.ui.outputFileLineEdit.text()

        # Load in a file to play
        file = QDir.current().filePath(output)
        # self.processedVideo.videoPlayerProc.setMedia(QMediaContent(QUrl.fromLocalFile(file)))
        # self.processedVideo.videoPlayerProc.setVideoOutput(self.ui.vidWidgetProc)
        # self.processedVideo.player.show()

