

from viewVideos import viewVid
from analyseVideo import analyseVid
from procVid import procVid

from PyQt5.QtCore import QDir, Qt, QUrl, pyqtSignal, pyqtSlot
# from PyQt5.QtMultimedia import QMediaContent, QMediaPlayer
# from PyQt5.QtMultimediaWidgets import QVideoWidget
from PyQt5.QtWidgets import (QApplication, QFileDialog, QHBoxLayout, QLabel,
        QPushButton, QSizePolicy, QSlider, QStyle, QVBoxLayout, QWidget, QMessageBox)
from PyQt5.QtWidgets import QMainWindow,QWidget, QPushButton, QAction, QFrame
from PyQt5.QtGui import QIcon
from wildlife_detector_window import Ui_wildLifeDetector
import sys
import argparse
import detection
import math
import os

# The class that handles the application itself
class ApplicationWindow(QMainWindow):

    def __init__(self):
        # Create the Qt5 application backend
        super(ApplicationWindow, self).__init__()

        # Load in and display the UI
        self.ui = Ui_wildLifeDetector()
        self.ui.setupUi(self)

        # self.windowChanged = pyqtSignal(int, name='windowChanged')

        self.windowChange = WindowChange()

        self.procVideo = procVid(self, self.windowChange)
        self.analyseVideo = analyseVid(self, self.procVideo)


        
        # self.example.ConnectTrigger()


    def resizeEvent(self, resizeEvent):
        # print ("hello")
        self.windowChange.resize.emit()


class WindowChange(QWidget):
    resize = pyqtSignal()

    # def ConnectTrigger(self):
    #     # Connect the trigger signal to a slot.
    #     self.windowChanged.connect(self.HandleTrigger)

    #     # Emit the signal.
    #     self.windowChanged.emit()

    # def HandleTrigger(self):
    #     # Show that the slot has been called.
    #     print("trigger signal received")

# class customFrame(QFrame):

#     def __init__(self, parent=None):
#         QFrame.__init__(self, parent)

#     def resizeEvent(self, evt=None):
#         self.emit(SIGNAL("resize()"))

# The "main()" function, like a C program
def main(params):

    if not params.supress:
        # Start GUI
        print("Loading application...")
        app = QApplication(sys.argv)
        application = ApplicationWindow()
        print("Application loaded.")
        application.show()
        sys.exit(app.exec_())
    else:
        # Run in command line
        path = os.path.abspath(params.path)
        split = os.path.split(path)

        if params.path == "":
            print("\n\nPlease suppy a path to a video when running in headless mode using the '-p' or '--path' option")
            return
        else:
            # Check video path
            vid = detection.VideoControl(path)
            if not vid.valid:
                print("\n\nThe path provided does not equate to a valid video, please review path and try again")
                return

            # Check save name
            if params.name == '':
                outPath = path.split('.')
                fileNameOut = outPath[0] + '_analysed.mp4'
            else:
                outName = params.name + '.mp4'
                fileNameOut = os.path.join(split[0], outName)

            # Check animals to be analysed
            animal = 2
            animalText = 'Analysis will search video frames for whales and dolphins'
            if params.ignoreDolphins and params.ignoreWhales:
                print("\n\nignoreDolphins and ignoreWhales can not both be set. At least one animal type is required for analysis")
                return
            elif params.ignoreDolphins:
                animal = 0
                animalText = 'Analysis will search video frames for whales'
            elif params.ignoreWhales:
                animal = 1
                animalText = 'Analysis will search video frames for dolphins'

            # Print information
            print('\n\nAnalysed video will be saved to: {}'.format(fileNameOut))
            print('\n{}'.format(animalText))
            vid.PrintVideoStats()

            startFrame = 0
            endFrame = int(vid.frameCount - 1)

            if params.fStart > 0 and params.fStart < endFrame:
                startFrame = params.fStart
            
            if params.fEnd > startFrame and params.fEnd < endFrame:
                endFrame = params.fEnd

            if params.fEnd == 0 and params.fTotal > 0 and startFrame + params.fTotal < endFrame:
                endFrame = startFrame + params.fTotal - 1

            print("\nAnalysing video from frame {} to frame {} for a total of {} frames".format(startFrame, endFrame, endFrame-startFrame+1))
            print("This analysis is estimated to take {}".format(CalculateTime(endFrame-startFrame+1)))

            # Prompt user to continue
            print("\n\nWould you like to continue with the analysis? Enter (y/Y) to proceed")
            key = sys.stdin.read(1)

            if (key == 'y' or key == 'Y'):
                print("\n\nPerforming analysis... Please wait")

                boxes = [None] * (int(vid.frameCount) + 1)
                detection.LoadModel('resnet50_best.h5')
                detection.DetectVideo(vid, boxes, output = fileNameOut, save = True, frameStart = startFrame, frameEnd = endFrame, animal = animal)
                vid.Close()
                print("\n\nAnalysis complete.")

            print("System exiting.")

            

      
      
def CalculateTime(frames):

    totalTime = 4.8 * frames + 20

    hours = int(math.floor(totalTime / 3600))
    totalTime -= (hours * 3600)
    minutes = int(math.floor(totalTime / 60))
    totalTime -= (minutes * 60)
    totalTime = int(round(totalTime))

    if hours > 0:
        timeStr = '{:d}:{:02d}:{:02d}'.format(hours, minutes, totalTime)
    else:
        timeStr = '{:d}:{:02d}'.format(minutes, totalTime)
    
    return timeStr


def args():
    parser = argparse.ArgumentParser()
    parser.add_argument("-s", "--supress", required = False, default = False, action='store_true',
                    help = "Supress the GUI window and run in command line")
    parser.add_argument("-p", "--path", required = False, default = "", 
                    help = "Path to video to be analysed")
    parser.add_argument("-n", "--name", required = False, default = "", 
                    help = "Name of the analysed video to be saved")
    parser.add_argument("-fs", "--fStart", type = int, required = False, default = 0,
                    help = "Specify the frame number to start analysing at")
    parser.add_argument("-fe", "--fEnd", type = int, required = False, default = 0, 
                    help = "Specify the frame number to finish (inclusive) analysing at")
    parser.add_argument("-ft", "--fTotal", type = int, required = False, default = 0,
                    help = "Specify the total number of frames to analyse")
    parser.add_argument("-id", "--ignoreDolphins", required = False, default = False, action='store_true', 
                    help = "Do not detect dolphins")
    parser.add_argument("-iw", "--ignoreWhales", required = False, default = False, action='store_true', 
                    help = "Do not detect whales")


    params = parser.parse_args()
    
    return params

# Provides a start point for out code
if __name__ == "__main__":
    params = args()
    main(params)


