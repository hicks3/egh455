# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'wildlife_detector_window.ui'
#
# Created by: PyQt5 UI code generator 5.11.3
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_wildLifeDetector(object):
    def setupUi(self, wildLifeDetector):
        wildLifeDetector.setObjectName("wildLifeDetector")
        wildLifeDetector.resize(1100, 650)
        wildLifeDetector.setMinimumSize(QtCore.QSize(1000, 500))
        icon = QtGui.QIcon()
        icon.addPixmap(QtGui.QPixmap("flower.svg"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        wildLifeDetector.setWindowIcon(icon)
        wildLifeDetector.setWindowOpacity(1.0)
        wildLifeDetector.setToolTipDuration(15)
        wildLifeDetector.setStyleSheet("background: white;")
        self.centralwidget = QtWidgets.QWidget(wildLifeDetector)
        self.centralwidget.setObjectName("centralwidget")
        self.gridLayout_5 = QtWidgets.QGridLayout(self.centralwidget)
        self.gridLayout_5.setObjectName("gridLayout_5")
        self.gridLayout_2 = QtWidgets.QGridLayout()
        self.gridLayout_2.setObjectName("gridLayout_2")
        self.tabWidget = QtWidgets.QTabWidget(self.centralwidget)
        self.tabWidget.setEnabled(True)
        self.tabWidget.setMinimumSize(QtCore.QSize(0, 0))
        font = QtGui.QFont()
        font.setFamily("Segoe UI Light")
        font.setPointSize(18)
        self.tabWidget.setFont(font)
        self.tabWidget.setStyleSheet("QTabWidget::pane {\n"
"    border: 0px solid #000;\n"
"}\n"
"\n"
"\n"
"QTabWidget::tab-bar {\n"
"    alignment: center;\n"
"}\n"
"\n"
"QTabBar::tab {\n"
"    background: #fff;\n"
"    border: 0px solid #000;\n"
"    border-top-left-radius: 2px;\n"
"    border-top-right-radius: 2px;\n"
"    width: 280px;\n"
"    height: 30px;\n"
"    padding: 2px;\n"
"}\n"
"\n"
"QTabBar::tab:selected {\n"
"    color: #000;\n"
"}\n"
"\n"
"QTabBar::tab:pressed {\n"
"    color: #111;\n"
"}\n"
"\n"
"QTabBar::tab:!selected {\n"
"    color: #999;\n"
"}\n"
"\n"
"QTabBar::tab::disabled {\n"
"    width: 0;\n"
"     height: 0;\n"
"     margin: 0; \n"
"    padding: 0;\n"
"     border: none;\n"
"}")
        self.tabWidget.setTabPosition(QtWidgets.QTabWidget.North)
        self.tabWidget.setTabShape(QtWidgets.QTabWidget.Rounded)
        self.tabWidget.setIconSize(QtCore.QSize(32, 32))
        self.tabWidget.setElideMode(QtCore.Qt.ElideNone)
        self.tabWidget.setDocumentMode(False)
        self.tabWidget.setTabBarAutoHide(False)
        self.tabWidget.setObjectName("tabWidget")
        self.analyseTab = QtWidgets.QWidget()
        self.analyseTab.setObjectName("analyseTab")
        self.gridLayout_9 = QtWidgets.QGridLayout(self.analyseTab)
        self.gridLayout_9.setObjectName("gridLayout_9")
        spacerItem = QtWidgets.QSpacerItem(20, 20, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Minimum)
        self.gridLayout_9.addItem(spacerItem, 0, 1, 1, 1)
        self.scrollArea = QtWidgets.QScrollArea(self.analyseTab)
        self.scrollArea.setMinimumSize(QtCore.QSize(650, 0))
        self.scrollArea.setMaximumSize(QtCore.QSize(1800, 16777215))
        self.scrollArea.setStyleSheet("QScrollArea {\n"
"    border: 0px solid #000;\n"
"}\n"
"\n"
"QScrollBar:vertical {\n"
"    border: 0px solid grey;\n"
"    border-radius: 5px;\n"
"    background: rgb(222, 222, 222);\n"
"    width: 10px;\n"
"    margin: 22px 0 22px 0;\n"
"}\n"
"\n"
" QScrollBar::add-line:vertical {\n"
"     border: 0px solid grey;\n"
"     background: #32CC99;\n"
"     height: 00px;\n"
"     subcontrol-position: bottom;\n"
"     subcontrol-origin: margin;\n"
" }\n"
"\n"
" QScrollBar::sub-line:vertical {\n"
"     border: 0px solid grey;\n"
"     background: #32CC99;\n"
"     height: 0px;\n"
"     subcontrol-position: top;\n"
"     subcontrol-origin: margin;\n"
" }\n"
" QScrollBar::handle:vertical {\n"
"    border-radius: 5px;\n"
"    background: rgb(190, 190, 190);\n"
"    min-height: 20px;\n"
" }\n"
"\n"
"\n"
"\n"
"")
        self.scrollArea.setHorizontalScrollBarPolicy(QtCore.Qt.ScrollBarAlwaysOff)
        self.scrollArea.setSizeAdjustPolicy(QtWidgets.QAbstractScrollArea.AdjustToContents)
        self.scrollArea.setWidgetResizable(True)
        self.scrollArea.setObjectName("scrollArea")
        self.scrollAreaWidgetContents = QtWidgets.QWidget()
        self.scrollAreaWidgetContents.setGeometry(QtCore.QRect(0, 0, 1052, 633))
        self.scrollAreaWidgetContents.setObjectName("scrollAreaWidgetContents")
        self.gridLayout_8 = QtWidgets.QGridLayout(self.scrollAreaWidgetContents)
        self.gridLayout_8.setObjectName("gridLayout_8")
        self.label_3 = QtWidgets.QLabel(self.scrollAreaWidgetContents)
        self.label_3.setMinimumSize(QtCore.QSize(0, 25))
        self.label_3.setMaximumSize(QtCore.QSize(16777215, 25))
        font = QtGui.QFont()
        font.setFamily("Segoe UI Light")
        font.setPointSize(14)
        font.setBold(False)
        font.setItalic(True)
        font.setWeight(50)
        self.label_3.setFont(font)
        self.label_3.setObjectName("label_3")
        self.gridLayout_8.addWidget(self.label_3, 3, 1, 1, 1)
        self.gridLayout_6 = QtWidgets.QGridLayout()
        self.gridLayout_6.setObjectName("gridLayout_6")
        self.analyseButton = QtWidgets.QPushButton(self.scrollAreaWidgetContents)
        self.analyseButton.setEnabled(False)
        self.analyseButton.setMinimumSize(QtCore.QSize(200, 37))
        self.analyseButton.setMaximumSize(QtCore.QSize(16777215, 37))
        font = QtGui.QFont()
        font.setFamily("Segoe UI")
        font.setPointSize(14)
        self.analyseButton.setFont(font)
        self.analyseButton.setStyleSheet("QPushButton {\n"
"    border-radius: 4px;\n"
"    background-color: rgb(51, 157, 255);\n"
"    color: white;\n"
"}\n"
"\n"
"QPushButton:hover {\n"
"    background-color: #2b88d9;\n"
"}\n"
"\n"
"QPushButton:pressed {\n"
"    background-color: rgb(25, 84, 132);\n"
"}\n"
"\n"
"QPushButton:disabled {\n"
"    background-color: rgb(24, 85, 131);\n"
"}")
        self.analyseButton.setObjectName("analyseButton")
        self.gridLayout_6.addWidget(self.analyseButton, 0, 1, 1, 1)
        spacerItem1 = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.gridLayout_6.addItem(spacerItem1, 0, 0, 1, 1)
        spacerItem2 = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.gridLayout_6.addItem(spacerItem2, 0, 2, 1, 1)
        self.gridLayout_8.addLayout(self.gridLayout_6, 21, 1, 1, 1)
        self.horizontalLayout_3 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_3.setSizeConstraint(QtWidgets.QLayout.SetDefaultConstraint)
        self.horizontalLayout_3.setObjectName("horizontalLayout_3")
        self.InputFileLineEditAnalysis = QtWidgets.QLineEdit(self.scrollAreaWidgetContents)
        self.InputFileLineEditAnalysis.setEnabled(False)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.InputFileLineEditAnalysis.sizePolicy().hasHeightForWidth())
        self.InputFileLineEditAnalysis.setSizePolicy(sizePolicy)
        self.InputFileLineEditAnalysis.setMinimumSize(QtCore.QSize(450, 35))
        self.InputFileLineEditAnalysis.setMaximumSize(QtCore.QSize(16777215, 35))
        font = QtGui.QFont()
        font.setFamily("Segoe UI Light")
        font.setPointSize(12)
        self.InputFileLineEditAnalysis.setFont(font)
        self.InputFileLineEditAnalysis.setStyleSheet("QLineEdit {\n"
"    border-radius: 4px \n"
"    border: 1px solid rgb(231, 231, 231);\n"
"    margin: 20px;\n"
"}\n"
"")
        self.InputFileLineEditAnalysis.setObjectName("InputFileLineEditAnalysis")
        self.horizontalLayout_3.addWidget(self.InputFileLineEditAnalysis)
        self.InputFileButtonAnalysis = QtWidgets.QPushButton(self.scrollAreaWidgetContents)
        self.InputFileButtonAnalysis.setMinimumSize(QtCore.QSize(172, 37))
        self.InputFileButtonAnalysis.setMaximumSize(QtCore.QSize(172, 37))
        font = QtGui.QFont()
        font.setFamily("Segoe UI Semibold")
        font.setPointSize(12)
        font.setBold(True)
        font.setWeight(75)
        self.InputFileButtonAnalysis.setFont(font)
        self.InputFileButtonAnalysis.setStyleSheet("QPushButton {\n"
"    border-radius: 4px;\n"
"    background-color: rgb(51, 157, 255);\n"
"    color: white;\n"
"}\n"
"\n"
"QPushButton:hover {\n"
"    background-color: #2b88d9;\n"
"}\n"
"\n"
"QPushButton:pressed {\n"
"    background-color: rgb(25, 84, 132);\n"
"}\n"
"\n"
"QPushButton:disabled {\n"
"    background-color: rgb(24, 85, 131);\n"
"}\n"
"")
        self.InputFileButtonAnalysis.setFlat(True)
        self.InputFileButtonAnalysis.setObjectName("InputFileButtonAnalysis")
        self.horizontalLayout_3.addWidget(self.InputFileButtonAnalysis)
        self.gridLayout_8.addLayout(self.horizontalLayout_3, 1, 1, 1, 1)
        self.progressBar = QtWidgets.QProgressBar(self.scrollAreaWidgetContents)
        font = QtGui.QFont()
        font.setFamily("Segoe UI Light")
        font.setPointSize(14)
        self.progressBar.setFont(font)
        self.progressBar.setStyleSheet("QProgressBar {\n"
"    border: 2px solid grey;\n"
"    border-radius: 5px;\n"
"    text-align: center;\n"
"}\n"
"\n"
"QProgressBar::chunk {\n"
"    background-color: #05B8CC;\n"
"    width: 20px;\n"
"}")
        self.progressBar.setProperty("value", 24)
        self.progressBar.setTextVisible(True)
        self.progressBar.setObjectName("progressBar")
        self.gridLayout_8.addWidget(self.progressBar, 20, 1, 1, 1)
        self.checkBoxWhole = QtWidgets.QCheckBox(self.scrollAreaWidgetContents)
        self.checkBoxWhole.setMaximumSize(QtCore.QSize(16777215, 21))
        font = QtGui.QFont()
        font.setFamily("Segoe UI Light")
        font.setPointSize(12)
        self.checkBoxWhole.setFont(font)
        self.checkBoxWhole.setStyleSheet("QCheckBox {\n"
"    spacing: 5px;\n"
"}\n"
"\n"
"QCheckBox::indicator {\n"
"    width: 20px;\n"
"    height: 13px;\n"
"}\n"
"\n"
"QCheckBox::indicator:checked {\n"
"    color: rgb(115, 115, 115)\n"
"}\n"
"")
        self.checkBoxWhole.setChecked(True)
        self.checkBoxWhole.setAutoExclusive(True)
        self.checkBoxWhole.setObjectName("checkBoxWhole")
        self.gridLayout_8.addWidget(self.checkBoxWhole, 10, 1, 1, 1)
        self.gridLayout_14 = QtWidgets.QGridLayout()
        self.gridLayout_14.setObjectName("gridLayout_14")
        self.frameStart = QtWidgets.QLineEdit(self.scrollAreaWidgetContents)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Fixed, QtWidgets.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.frameStart.sizePolicy().hasHeightForWidth())
        self.frameStart.setSizePolicy(sizePolicy)
        self.frameStart.setMinimumSize(QtCore.QSize(0, 30))
        self.frameStart.setMaximumSize(QtCore.QSize(60, 30))
        font = QtGui.QFont()
        font.setFamily("Segoe UI Light")
        font.setPointSize(12)
        self.frameStart.setFont(font)
        self.frameStart.setStyleSheet("QLineEdit {\n"
"    border-radius: 4px \n"
"    border: 1px solid rgb(231, 231, 231);\n"
"    margin: 20px;\n"
"}\n"
"")
        self.frameStart.setFrame(True)
        self.frameStart.setAlignment(QtCore.Qt.AlignCenter)
        self.frameStart.setObjectName("frameStart")
        self.gridLayout_14.addWidget(self.frameStart, 0, 1, 1, 1)
        self.checkBoxSection = QtWidgets.QCheckBox(self.scrollAreaWidgetContents)
        self.checkBoxSection.setMaximumSize(QtCore.QSize(16777215, 30))
        font = QtGui.QFont()
        font.setFamily("Segoe UI Light")
        font.setPointSize(12)
        self.checkBoxSection.setFont(font)
        self.checkBoxSection.setStyleSheet("QCheckBox {\n"
"    spacing: 5px;\n"
"}\n"
"\n"
"QCheckBox::indicator {\n"
"    width: 20px;\n"
"    height: 13px;\n"
"}\n"
"\n"
"QCheckBox::indicator:checked {\n"
"    color: rgb(115, 115, 115)\n"
"}\n"
"")
        self.checkBoxSection.setChecked(False)
        self.checkBoxSection.setAutoExclusive(True)
        self.checkBoxSection.setObjectName("checkBoxSection")
        self.gridLayout_14.addWidget(self.checkBoxSection, 0, 0, 1, 1)
        self.labelTo = QtWidgets.QLabel(self.scrollAreaWidgetContents)
        self.labelTo.setMaximumSize(QtCore.QSize(16777215, 30))
        font = QtGui.QFont()
        font.setFamily("Segoe UI Light")
        font.setPointSize(12)
        self.labelTo.setFont(font)
        self.labelTo.setObjectName("labelTo")
        self.gridLayout_14.addWidget(self.labelTo, 0, 2, 1, 1)
        self.frameEnd = QtWidgets.QLineEdit(self.scrollAreaWidgetContents)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Fixed, QtWidgets.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.frameEnd.sizePolicy().hasHeightForWidth())
        self.frameEnd.setSizePolicy(sizePolicy)
        self.frameEnd.setMinimumSize(QtCore.QSize(0, 30))
        self.frameEnd.setMaximumSize(QtCore.QSize(60, 30))
        font = QtGui.QFont()
        font.setFamily("Segoe UI Light")
        font.setPointSize(12)
        self.frameEnd.setFont(font)
        self.frameEnd.setStyleSheet("QLineEdit {\n"
"    border-radius: 4px \n"
"    border: 1px solid rgb(231, 231, 231);\n"
"    margin: 20px;\n"
"}\n"
"")
        self.frameEnd.setAlignment(QtCore.Qt.AlignCenter)
        self.frameEnd.setObjectName("frameEnd")
        self.gridLayout_14.addWidget(self.frameEnd, 0, 3, 1, 1)
        spacerItem3 = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.gridLayout_14.addItem(spacerItem3, 0, 4, 1, 1)
        self.gridLayout_8.addLayout(self.gridLayout_14, 12, 1, 1, 1)
        self.horizontalLayout_2 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_2.setSizeConstraint(QtWidgets.QLayout.SetDefaultConstraint)
        self.horizontalLayout_2.setObjectName("horizontalLayout_2")
        self.outputFileLineEditAnalysis = QtWidgets.QLineEdit(self.scrollAreaWidgetContents)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.outputFileLineEditAnalysis.sizePolicy().hasHeightForWidth())
        self.outputFileLineEditAnalysis.setSizePolicy(sizePolicy)
        self.outputFileLineEditAnalysis.setMinimumSize(QtCore.QSize(450, 35))
        self.outputFileLineEditAnalysis.setMaximumSize(QtCore.QSize(16777215, 35))
        font = QtGui.QFont()
        font.setFamily("Segoe UI Light")
        font.setPointSize(12)
        self.outputFileLineEditAnalysis.setFont(font)
        self.outputFileLineEditAnalysis.setStyleSheet("QLineEdit {\n"
"    border-radius: 4px \n"
"    border: 1px solid rgb(231, 231, 231);\n"
"    margin: 20px;\n"
"}\n"
"")
        self.outputFileLineEditAnalysis.setObjectName("outputFileLineEditAnalysis")
        self.horizontalLayout_2.addWidget(self.outputFileLineEditAnalysis)
        self.outputFileButtonAnalysis = QtWidgets.QPushButton(self.scrollAreaWidgetContents)
        self.outputFileButtonAnalysis.setMinimumSize(QtCore.QSize(172, 37))
        self.outputFileButtonAnalysis.setMaximumSize(QtCore.QSize(172, 37))
        font = QtGui.QFont()
        font.setFamily("Segoe UI Semibold")
        font.setPointSize(12)
        font.setBold(True)
        font.setWeight(75)
        self.outputFileButtonAnalysis.setFont(font)
        self.outputFileButtonAnalysis.setStyleSheet("QPushButton {\n"
"    border-radius: 4px;\n"
"    background-color: rgb(51, 157, 255);\n"
"    color: white;\n"
"}\n"
"\n"
"QPushButton:hover {\n"
"    background-color: #2b88d9;\n"
"}\n"
"\n"
"QPushButton:pressed {\n"
"    background-color: rgb(25, 84, 132);\n"
"}\n"
"\n"
"QPushButton:disabled {\n"
"    background-color: rgb(24, 85, 131);\n"
"}")
        self.outputFileButtonAnalysis.setFlat(True)
        self.outputFileButtonAnalysis.setObjectName("outputFileButtonAnalysis")
        self.horizontalLayout_2.addWidget(self.outputFileButtonAnalysis)
        self.gridLayout_8.addLayout(self.horizontalLayout_2, 4, 1, 1, 1)
        spacerItem4 = QtWidgets.QSpacerItem(20, 40, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Expanding)
        self.gridLayout_8.addItem(spacerItem4, 23, 1, 1, 1)
        self.horizontalLayout = QtWidgets.QHBoxLayout()
        self.horizontalLayout.setObjectName("horizontalLayout")
        self.checkBoxWhale = QtWidgets.QCheckBox(self.scrollAreaWidgetContents)
        self.checkBoxWhale.setMinimumSize(QtCore.QSize(120, 30))
        self.checkBoxWhale.setMaximumSize(QtCore.QSize(130, 30))
        font = QtGui.QFont()
        font.setFamily("Segoe UI Light")
        font.setPointSize(12)
        self.checkBoxWhale.setFont(font)
        self.checkBoxWhale.setStyleSheet("QCheckBox {\n"
"    spacing: 5px;\n"
"}\n"
"\n"
"QCheckBox::indicator {\n"
"    width: 20px;\n"
"    height: 13px;\n"
"}\n"
"\n"
"QCheckBox::indicator:checked {\n"
"    color: rgb(115, 115, 115)\n"
"}\n"
"")
        icon1 = QtGui.QIcon()
        icon1.addPixmap(QtGui.QPixmap("whale.svg"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.checkBoxWhale.setIcon(icon1)
        self.checkBoxWhale.setIconSize(QtCore.QSize(32, 28))
        self.checkBoxWhale.setChecked(True)
        self.checkBoxWhale.setObjectName("checkBoxWhale")
        self.horizontalLayout.addWidget(self.checkBoxWhale)
        self.checkBoxDolphin = QtWidgets.QCheckBox(self.scrollAreaWidgetContents)
        self.checkBoxDolphin.setMinimumSize(QtCore.QSize(130, 30))
        self.checkBoxDolphin.setMaximumSize(QtCore.QSize(130, 30))
        font = QtGui.QFont()
        font.setFamily("Segoe UI Light")
        font.setPointSize(12)
        self.checkBoxDolphin.setFont(font)
        self.checkBoxDolphin.setStyleSheet("QCheckBox {\n"
"    spacing: 5px;\n"
"}\n"
"\n"
"QCheckBox::indicator {\n"
"    width: 20px;\n"
"    height: 13px;\n"
"}\n"
"")
        icon2 = QtGui.QIcon()
        icon2.addPixmap(QtGui.QPixmap("dolphinIcon.svg"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.checkBoxDolphin.setIcon(icon2)
        self.checkBoxDolphin.setIconSize(QtCore.QSize(32, 28))
        self.checkBoxDolphin.setChecked(True)
        self.checkBoxDolphin.setObjectName("checkBoxDolphin")
        self.horizontalLayout.addWidget(self.checkBoxDolphin)
        self.checkBoxSwitch = QtWidgets.QCheckBox(self.scrollAreaWidgetContents)
        self.checkBoxSwitch.setEnabled(True)
        self.checkBoxSwitch.setMinimumSize(QtCore.QSize(0, 30))
        self.checkBoxSwitch.setMaximumSize(QtCore.QSize(16777215, 30))
        font = QtGui.QFont()
        font.setFamily("Segoe UI Light")
        font.setPointSize(12)
        self.checkBoxSwitch.setFont(font)
        self.checkBoxSwitch.setStyleSheet("QCheckBox {\n"
"    spacing: 5px;\n"
"}\n"
"\n"
"QCheckBox::indicator {\n"
"    width: 20px;\n"
"    height: 13px;\n"
"}\n"
"\n"
"QCheckBox::indicator:unchecked {\n"
"    color: rgb(115, 115, 115)\n"
"}\n"
"")
        self.checkBoxSwitch.setObjectName("checkBoxSwitch")
        self.horizontalLayout.addWidget(self.checkBoxSwitch)
        spacerItem5 = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.horizontalLayout.addItem(spacerItem5)
        self.gridLayout_8.addLayout(self.horizontalLayout, 7, 1, 1, 1)
        spacerItem6 = QtWidgets.QSpacerItem(200, 20, QtWidgets.QSizePolicy.Maximum, QtWidgets.QSizePolicy.Minimum)
        self.gridLayout_8.addItem(spacerItem6, 7, 0, 1, 1)
        spacerItem7 = QtWidgets.QSpacerItem(20, 15, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Minimum)
        self.gridLayout_8.addItem(spacerItem7, 8, 1, 1, 1)
        spacerItem8 = QtWidgets.QSpacerItem(200, 20, QtWidgets.QSizePolicy.Maximum, QtWidgets.QSizePolicy.Minimum)
        self.gridLayout_8.addItem(spacerItem8, 7, 2, 1, 1)
        self.labelOr = QtWidgets.QLabel(self.scrollAreaWidgetContents)
        self.labelOr.setMinimumSize(QtCore.QSize(0, 21))
        font = QtGui.QFont()
        font.setFamily("Segoe UI Light")
        font.setPointSize(12)
        font.setBold(False)
        font.setItalic(False)
        font.setWeight(50)
        self.labelOr.setFont(font)
        self.labelOr.setObjectName("labelOr")
        self.gridLayout_8.addWidget(self.labelOr, 11, 1, 1, 1)
        spacerItem9 = QtWidgets.QSpacerItem(20, 15, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Minimum)
        self.gridLayout_8.addItem(spacerItem9, 5, 1, 1, 1)
        self.label = QtWidgets.QLabel(self.scrollAreaWidgetContents)
        self.label.setMaximumSize(QtCore.QSize(16777215, 25))
        font = QtGui.QFont()
        font.setFamily("Segoe UI Light")
        font.setPointSize(14)
        font.setItalic(True)
        self.label.setFont(font)
        self.label.setObjectName("label")
        self.gridLayout_8.addWidget(self.label, 0, 1, 1, 1)
        self.gridLayout_17 = QtWidgets.QGridLayout()
        self.gridLayout_17.setObjectName("gridLayout_17")
        self.stopButton = QtWidgets.QPushButton(self.scrollAreaWidgetContents)
        self.stopButton.setMinimumSize(QtCore.QSize(200, 37))
        self.stopButton.setMaximumSize(QtCore.QSize(16777215, 37))
        font = QtGui.QFont()
        font.setFamily("Segoe UI")
        font.setPointSize(14)
        self.stopButton.setFont(font)
        self.stopButton.setStyleSheet("QPushButton {\n"
"    border-radius: 4px;\n"
"    background-color: #da4213;\n"
"    color: white;\n"
"}\n"
"\n"
"QPushButton:hover {\n"
"    background-color: #be3910;\n"
"}\n"
"\n"
"QPushButton:pressed {\n"
"    background-color:#9e2f0d;\n"
"}\n"
"\n"
"QPushButton:disabled {\n"
"    background-color: #732209;\n"
"}")
        self.stopButton.setObjectName("stopButton")
        self.gridLayout_17.addWidget(self.stopButton, 0, 1, 1, 1)
        spacerItem10 = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.gridLayout_17.addItem(spacerItem10, 0, 2, 1, 1)
        spacerItem11 = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.gridLayout_17.addItem(spacerItem11, 0, 0, 1, 1)
        self.gridLayout_8.addLayout(self.gridLayout_17, 22, 1, 1, 1)
        spacerItem12 = QtWidgets.QSpacerItem(20, 40, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Maximum)
        self.gridLayout_8.addItem(spacerItem12, 13, 1, 1, 1)
        spacerItem13 = QtWidgets.QSpacerItem(20, 30, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Minimum)
        self.gridLayout_8.addItem(spacerItem13, 18, 1, 1, 1)
        spacerItem14 = QtWidgets.QSpacerItem(30, 15, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Minimum)
        self.gridLayout_8.addItem(spacerItem14, 2, 1, 1, 1)
        self.labelFrames = QtWidgets.QLabel(self.scrollAreaWidgetContents)
        self.labelFrames.setMaximumSize(QtCore.QSize(16777215, 25))
        font = QtGui.QFont()
        font.setFamily("Segoe UI Light")
        font.setPointSize(14)
        font.setBold(False)
        font.setItalic(True)
        font.setWeight(50)
        self.labelFrames.setFont(font)
        self.labelFrames.setObjectName("labelFrames")
        self.gridLayout_8.addWidget(self.labelFrames, 9, 1, 1, 1)
        self.label_4 = QtWidgets.QLabel(self.scrollAreaWidgetContents)
        self.label_4.setMaximumSize(QtCore.QSize(16777215, 25))
        font = QtGui.QFont()
        font.setFamily("Segoe UI Light")
        font.setPointSize(14)
        font.setItalic(True)
        self.label_4.setFont(font)
        self.label_4.setObjectName("label_4")
        self.gridLayout_8.addWidget(self.label_4, 6, 1, 1, 1)
        self.gridLayout_7 = QtWidgets.QGridLayout()
        self.gridLayout_7.setObjectName("gridLayout_7")
        spacerItem15 = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.gridLayout_7.addItem(spacerItem15, 0, 0, 1, 1)
        self.labelCompletionTime = QtWidgets.QLabel(self.scrollAreaWidgetContents)
        font = QtGui.QFont()
        font.setFamily("Segoe UI Light")
        font.setPointSize(14)
        self.labelCompletionTime.setFont(font)
        self.labelCompletionTime.setObjectName("labelCompletionTime")
        self.gridLayout_7.addWidget(self.labelCompletionTime, 0, 1, 1, 1)
        spacerItem16 = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.gridLayout_7.addItem(spacerItem16, 0, 2, 1, 1)
        self.gridLayout_8.addLayout(self.gridLayout_7, 19, 1, 1, 1)
        self.scrollArea.setWidget(self.scrollAreaWidgetContents)
        self.gridLayout_9.addWidget(self.scrollArea, 1, 1, 1, 1)
        self.tabWidget.addTab(self.analyseTab, "")
        self.viewTab = QtWidgets.QWidget()
        self.viewTab.setEnabled(True)
        self.viewTab.setObjectName("viewTab")
        self.gridLayout_13 = QtWidgets.QGridLayout(self.viewTab)
        self.gridLayout_13.setObjectName("gridLayout_13")
        spacerItem17 = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Maximum, QtWidgets.QSizePolicy.Minimum)
        self.gridLayout_13.addItem(spacerItem17, 1, 3, 1, 1)
        spacerItem18 = QtWidgets.QSpacerItem(20, 20, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Fixed)
        self.gridLayout_13.addItem(spacerItem18, 0, 1, 1, 1)
        self.gridLayout_10 = QtWidgets.QGridLayout()
        self.gridLayout_10.setObjectName("gridLayout_10")
        self.gridLayout_12 = QtWidgets.QGridLayout()
        self.gridLayout_12.setObjectName("gridLayout_12")
        self.vidFrame = QtWidgets.QFrame(self.viewTab)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Expanding)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.vidFrame.sizePolicy().hasHeightForWidth())
        self.vidFrame.setSizePolicy(sizePolicy)
        self.vidFrame.setMinimumSize(QtCore.QSize(600, 301))
        self.vidFrame.setLayoutDirection(QtCore.Qt.LeftToRight)
        self.vidFrame.setAutoFillBackground(False)
        self.vidFrame.setFrameShape(QtWidgets.QFrame.StyledPanel)
        self.vidFrame.setFrameShadow(QtWidgets.QFrame.Raised)
        self.vidFrame.setObjectName("vidFrame")
        self.gridLayout = QtWidgets.QGridLayout(self.vidFrame)
        self.gridLayout.setContentsMargins(0, 0, 0, -1)
        self.gridLayout.setObjectName("gridLayout")
        self.procVideoLabel = QtWidgets.QLabel(self.vidFrame)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Fixed, QtWidgets.QSizePolicy.Expanding)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.procVideoLabel.sizePolicy().hasHeightForWidth())
        self.procVideoLabel.setSizePolicy(sizePolicy)
        self.procVideoLabel.setMinimumSize(QtCore.QSize(800, 0))
        font = QtGui.QFont()
        font.setFamily("Segoe UI Light")
        font.setPointSize(12)
        self.procVideoLabel.setFont(font)
        self.procVideoLabel.setFrameShape(QtWidgets.QFrame.NoFrame)
        self.procVideoLabel.setFrameShadow(QtWidgets.QFrame.Sunken)
        self.procVideoLabel.setScaledContents(False)
        self.procVideoLabel.setAlignment(QtCore.Qt.AlignCenter)
        self.procVideoLabel.setObjectName("procVideoLabel")
        self.gridLayout.addWidget(self.procVideoLabel, 0, 1, 1, 1)
        spacerItem19 = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.gridLayout.addItem(spacerItem19, 0, 0, 1, 1)
        spacerItem20 = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.gridLayout.addItem(spacerItem20, 0, 4, 1, 1)
        self.gridLayout_12.addWidget(self.vidFrame, 3, 1, 1, 1)
        self.gridLayout_18 = QtWidgets.QGridLayout()
        self.gridLayout_18.setObjectName("gridLayout_18")
        self.label_5 = QtWidgets.QLabel(self.viewTab)
        self.label_5.setMaximumSize(QtCore.QSize(16777215, 20))
        font = QtGui.QFont()
        font.setFamily("Segoe UI Light")
        font.setPointSize(10)
        font.setBold(False)
        font.setItalic(True)
        font.setWeight(50)
        self.label_5.setFont(font)
        self.label_5.setObjectName("label_5")
        self.gridLayout_18.addWidget(self.label_5, 1, 1, 1, 1)
        self.countBox = QtWidgets.QLabel(self.viewTab)
        self.countBox.setMaximumSize(QtCore.QSize(16777215, 25))
        font = QtGui.QFont()
        font.setFamily("Segoe UI Light")
        self.countBox.setFont(font)
        self.countBox.setObjectName("countBox")
        self.gridLayout_18.addWidget(self.countBox, 2, 1, 1, 1)
        self.sizeBox = QtWidgets.QLabel(self.viewTab)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Preferred, QtWidgets.QSizePolicy.Expanding)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.sizeBox.sizePolicy().hasHeightForWidth())
        self.sizeBox.setSizePolicy(sizePolicy)
        self.sizeBox.setMinimumSize(QtCore.QSize(130, 0))
        self.sizeBox.setMaximumSize(QtCore.QSize(200, 16777215))
        font = QtGui.QFont()
        font.setFamily("Segoe UI Light")
        font.setPointSize(8)
        self.sizeBox.setFont(font)
        self.sizeBox.setAlignment(QtCore.Qt.AlignLeading|QtCore.Qt.AlignLeft|QtCore.Qt.AlignTop)
        self.sizeBox.setObjectName("sizeBox")
        self.gridLayout_18.addWidget(self.sizeBox, 4, 1, 1, 1)
        spacerItem21 = QtWidgets.QSpacerItem(20, 5, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Maximum)
        self.gridLayout_18.addItem(spacerItem21, 3, 1, 1, 1)
        spacerItem22 = QtWidgets.QSpacerItem(20, 40, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Expanding)
        self.gridLayout_18.addItem(spacerItem22, 5, 1, 1, 1)
        spacerItem23 = QtWidgets.QSpacerItem(20, 40, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Expanding)
        self.gridLayout_18.addItem(spacerItem23, 0, 1, 1, 1)
        self.gridLayout_12.addLayout(self.gridLayout_18, 3, 2, 1, 1)
        self.gridLayout_10.addLayout(self.gridLayout_12, 2, 0, 1, 1)
        self.gridLayout_11 = QtWidgets.QGridLayout()
        self.gridLayout_11.setObjectName("gridLayout_11")
        self.SaveAsButton = QtWidgets.QPushButton(self.viewTab)
        self.SaveAsButton.setMinimumSize(QtCore.QSize(172, 37))
        self.SaveAsButton.setMaximumSize(QtCore.QSize(172, 37))
        font = QtGui.QFont()
        font.setFamily("Segoe UI Semibold")
        font.setPointSize(12)
        font.setBold(True)
        font.setWeight(75)
        self.SaveAsButton.setFont(font)
        self.SaveAsButton.setStyleSheet("QPushButton {\n"
"    border-radius: 4px;\n"
"    background-color: rgb(51, 157, 255);\n"
"    color: white;\n"
"}\n"
"\n"
"QPushButton:hover {\n"
"    background-color: #2b88d9;\n"
"}\n"
"\n"
"QPushButton:pressed {\n"
"    background-color: rgb(25, 84, 132);\n"
"}\n"
"\n"
"\n"
"QPushButton:disabled {\n"
"    background-color: rgb(24, 85, 131);\n"
"}")
        self.SaveAsButton.setFlat(True)
        self.SaveAsButton.setObjectName("SaveAsButton")
        self.gridLayout_11.addWidget(self.SaveAsButton, 0, 7, 1, 1)
        self.gridLayout_16 = QtWidgets.QGridLayout()
        self.gridLayout_16.setObjectName("gridLayout_16")
        self.horizontalLayout_6 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_6.setObjectName("horizontalLayout_6")
        self.label_8 = QtWidgets.QLabel(self.viewTab)
        self.label_8.setMinimumSize(QtCore.QSize(145, 0))
        font = QtGui.QFont()
        font.setFamily("Segoe UI Light")
        font.setPointSize(12)
        self.label_8.setFont(font)
        self.label_8.setObjectName("label_8")
        self.horizontalLayout_6.addWidget(self.label_8)
        self.checkBoxVidWhale = QtWidgets.QCheckBox(self.viewTab)
        self.checkBoxVidWhale.setMinimumSize(QtCore.QSize(130, 0))
        font = QtGui.QFont()
        font.setFamily("Segoe UI Light")
        font.setPointSize(12)
        self.checkBoxVidWhale.setFont(font)
        self.checkBoxVidWhale.setStyleSheet("QCheckBox {\n"
"    spacing: 5px;\n"
"}\n"
"\n"
"QCheckBox::indicator {\n"
"    width: 20px;\n"
"    height: 13px;\n"
"}\n"
"\n"
"QCheckBox::indicator:checked {\n"
"    color: rgb(115, 115, 115)\n"
"}\n"
"")
        self.checkBoxVidWhale.setIcon(icon1)
        self.checkBoxVidWhale.setIconSize(QtCore.QSize(32, 28))
        self.checkBoxVidWhale.setChecked(True)
        self.checkBoxVidWhale.setObjectName("checkBoxVidWhale")
        self.horizontalLayout_6.addWidget(self.checkBoxVidWhale)
        self.checkBoxVidDolphin = QtWidgets.QCheckBox(self.viewTab)
        self.checkBoxVidDolphin.setMinimumSize(QtCore.QSize(130, 0))
        font = QtGui.QFont()
        font.setFamily("Segoe UI Light")
        font.setPointSize(12)
        self.checkBoxVidDolphin.setFont(font)
        self.checkBoxVidDolphin.setStyleSheet("QCheckBox {\n"
"    spacing: 5px;\n"
"}\n"
"\n"
"QCheckBox::indicator {\n"
"    width: 20px;\n"
"    height: 13px;\n"
"}\n"
"")
        self.checkBoxVidDolphin.setIcon(icon2)
        self.checkBoxVidDolphin.setIconSize(QtCore.QSize(32, 28))
        self.checkBoxVidDolphin.setChecked(True)
        self.checkBoxVidDolphin.setObjectName("checkBoxVidDolphin")
        self.horizontalLayout_6.addWidget(self.checkBoxVidDolphin)
        self.checkBoxVidSwitch = QtWidgets.QCheckBox(self.viewTab)
        self.checkBoxVidSwitch.setEnabled(True)
        font = QtGui.QFont()
        font.setFamily("Segoe UI Light")
        font.setPointSize(12)
        self.checkBoxVidSwitch.setFont(font)
        self.checkBoxVidSwitch.setStyleSheet("QCheckBox {\n"
"    spacing: 5px;\n"
"}\n"
"\n"
"QCheckBox::indicator {\n"
"    width: 20px;\n"
"    height: 13px;\n"
"}\n"
"\n"
"QCheckBox::indicator:unchecked {\n"
"    color: rgb(115, 115, 115)\n"
"}\n"
"")
        self.checkBoxVidSwitch.setObjectName("checkBoxVidSwitch")
        self.horizontalLayout_6.addWidget(self.checkBoxVidSwitch)
        spacerItem24 = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.horizontalLayout_6.addItem(spacerItem24)
        self.gridLayout_16.addLayout(self.horizontalLayout_6, 0, 0, 1, 1)
        self.horizontalLayout_4 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_4.setObjectName("horizontalLayout_4")
        self.label_2 = QtWidgets.QLabel(self.viewTab)
        self.label_2.setMinimumSize(QtCore.QSize(147, 0))
        self.label_2.setMaximumSize(QtCore.QSize(147, 21))
        font = QtGui.QFont()
        font.setFamily("Segoe UI Light")
        font.setPointSize(12)
        self.label_2.setFont(font)
        self.label_2.setObjectName("label_2")
        self.horizontalLayout_4.addWidget(self.label_2)
        self.checkBoxOverlayName = QtWidgets.QCheckBox(self.viewTab)
        font = QtGui.QFont()
        font.setFamily("Segoe UI Light")
        font.setPointSize(12)
        self.checkBoxOverlayName.setFont(font)
        self.checkBoxOverlayName.setChecked(True)
        self.checkBoxOverlayName.setObjectName("checkBoxOverlayName")
        self.horizontalLayout_4.addWidget(self.checkBoxOverlayName)
        self.checkBoxOverlayConfidence = QtWidgets.QCheckBox(self.viewTab)
        self.checkBoxOverlayConfidence.setMinimumSize(QtCore.QSize(0, 0))
        self.checkBoxOverlayConfidence.setMaximumSize(QtCore.QSize(16777215, 21))
        font = QtGui.QFont()
        font.setFamily("Segoe UI Light")
        font.setPointSize(12)
        self.checkBoxOverlayConfidence.setFont(font)
        self.checkBoxOverlayConfidence.setChecked(True)
        self.checkBoxOverlayConfidence.setObjectName("checkBoxOverlayConfidence")
        self.horizontalLayout_4.addWidget(self.checkBoxOverlayConfidence)
        self.checkBoxOverlaySize = QtWidgets.QCheckBox(self.viewTab)
        font = QtGui.QFont()
        font.setFamily("Segoe UI Light")
        font.setPointSize(12)
        self.checkBoxOverlaySize.setFont(font)
        self.checkBoxOverlaySize.setObjectName("checkBoxOverlaySize")
        self.horizontalLayout_4.addWidget(self.checkBoxOverlaySize)
        self.checkBoxOverlayCount = QtWidgets.QCheckBox(self.viewTab)
        self.checkBoxOverlayCount.setMinimumSize(QtCore.QSize(0, 0))
        self.checkBoxOverlayCount.setMaximumSize(QtCore.QSize(16777215, 21))
        font = QtGui.QFont()
        font.setFamily("Segoe UI Light")
        font.setPointSize(12)
        self.checkBoxOverlayCount.setFont(font)
        self.checkBoxOverlayCount.setObjectName("checkBoxOverlayCount")
        self.horizontalLayout_4.addWidget(self.checkBoxOverlayCount)
        spacerItem25 = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.horizontalLayout_4.addItem(spacerItem25)
        self.gridLayout_16.addLayout(self.horizontalLayout_4, 1, 0, 1, 1)
        self.gridLayout_11.addLayout(self.gridLayout_16, 0, 5, 1, 1)
        spacerItem26 = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.gridLayout_11.addItem(spacerItem26, 0, 6, 1, 1)
        self.gridLayout_10.addLayout(self.gridLayout_11, 0, 0, 1, 1)
        self.gridLayout_3 = QtWidgets.QGridLayout()
        self.gridLayout_3.setObjectName("gridLayout_3")
        self.frameBackward = QtWidgets.QPushButton(self.viewTab)
        self.frameBackward.setMinimumSize(QtCore.QSize(25, 22))
        self.frameBackward.setMaximumSize(QtCore.QSize(25, 22))
        self.frameBackward.setStyleSheet("QPushButton {\n"
"    border-radius: 4px;\n"
"    background-color: rgb(51, 157, 255);\n"
"    color: white;\n"
"}\n"
"\n"
"QPushButton:hover {\n"
"    background-color: #2b88d9;\n"
"}\n"
"\n"
"QPushButton:pressed {\n"
"    background-color: rgb(25, 84, 132);\n"
"}\n"
"\n"
"\n"
"QPushButton:disabled {\n"
"    background-color: rgb(24, 85, 131);\n"
"}")
        self.frameBackward.setText("")
        self.frameBackward.setObjectName("frameBackward")
        self.gridLayout_3.addWidget(self.frameBackward, 0, 1, 1, 1)
        self.playButtonProc = QtWidgets.QPushButton(self.viewTab)
        self.playButtonProc.setMinimumSize(QtCore.QSize(25, 22))
        self.playButtonProc.setMaximumSize(QtCore.QSize(25, 22))
        self.playButtonProc.setStyleSheet("QPushButton {\n"
"    border-radius: 4px;\n"
"    background-color: rgb(51, 157, 255);\n"
"    color: white;\n"
"}\n"
"\n"
"QPushButton:hover {\n"
"    background-color: #2b88d9;\n"
"}\n"
"\n"
"QPushButton:pressed {\n"
"    background-color: rgb(25, 84, 132);\n"
"}\n"
"\n"
"\n"
"QPushButton:disabled {\n"
"    background-color: rgb(24, 85, 131);\n"
"}")
        self.playButtonProc.setText("")
        self.playButtonProc.setObjectName("playButtonProc")
        self.gridLayout_3.addWidget(self.playButtonProc, 0, 0, 1, 1)
        self.frameForward = QtWidgets.QPushButton(self.viewTab)
        self.frameForward.setMinimumSize(QtCore.QSize(25, 22))
        self.frameForward.setMaximumSize(QtCore.QSize(25, 22))
        self.frameForward.setStyleSheet("QPushButton {\n"
"    border-radius: 4px;\n"
"    background-color: rgb(51, 157, 255);\n"
"    color: white;\n"
"}\n"
"\n"
"QPushButton:hover {\n"
"    background-color: #2b88d9;\n"
"}\n"
"\n"
"QPushButton:pressed {\n"
"    background-color: rgb(25, 84, 132);\n"
"}\n"
"\n"
"\n"
"QPushButton:disabled {\n"
"    background-color: rgb(24, 85, 131);\n"
"}")
        self.frameForward.setText("")
        self.frameForward.setObjectName("frameForward")
        self.gridLayout_3.addWidget(self.frameForward, 0, 2, 1, 1)
        self.vidSliderProc = QtWidgets.QSlider(self.viewTab)
        self.vidSliderProc.setEnabled(True)
        self.vidSliderProc.setOrientation(QtCore.Qt.Horizontal)
        self.vidSliderProc.setObjectName("vidSliderProc")
        self.gridLayout_3.addWidget(self.vidSliderProc, 0, 3, 1, 1)
        self.gridLayout_10.addLayout(self.gridLayout_3, 3, 0, 1, 1)
        self.gridLayout_15 = QtWidgets.QGridLayout()
        self.gridLayout_15.setObjectName("gridLayout_15")
        self.gridLayout_10.addLayout(self.gridLayout_15, 1, 0, 1, 1)
        self.gridLayout_13.addLayout(self.gridLayout_10, 1, 1, 1, 1)
        spacerItem27 = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Maximum, QtWidgets.QSizePolicy.Minimum)
        self.gridLayout_13.addItem(spacerItem27, 1, 0, 1, 1)
        self.tabWidget.addTab(self.viewTab, "")
        self.gridLayout_2.addWidget(self.tabWidget, 1, 0, 1, 1)
        self.gridLayout_5.addLayout(self.gridLayout_2, 1, 0, 1, 1)
        self.gridLayout_4 = QtWidgets.QGridLayout()
        self.gridLayout_4.setObjectName("gridLayout_4")
        self.label_6 = QtWidgets.QLabel(self.centralwidget)
        self.label_6.setMinimumSize(QtCore.QSize(650, 0))
        font = QtGui.QFont()
        font.setFamily("Segoe UI Light")
        font.setPointSize(15)
        font.setBold(True)
        font.setItalic(True)
        font.setWeight(75)
        self.label_6.setFont(font)
        self.label_6.setObjectName("label_6")
        self.gridLayout_4.addWidget(self.label_6, 0, 1, 1, 1)
        spacerItem28 = QtWidgets.QSpacerItem(200, 20, QtWidgets.QSizePolicy.Maximum, QtWidgets.QSizePolicy.Minimum)
        self.gridLayout_4.addItem(spacerItem28, 0, 2, 1, 1)
        spacerItem29 = QtWidgets.QSpacerItem(200, 20, QtWidgets.QSizePolicy.Maximum, QtWidgets.QSizePolicy.Minimum)
        self.gridLayout_4.addItem(spacerItem29, 0, 0, 1, 1)
        self.gridLayout_5.addLayout(self.gridLayout_4, 0, 0, 1, 1)
        spacerItem30 = QtWidgets.QSpacerItem(20, 20, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Maximum)
        self.gridLayout_5.addItem(spacerItem30, 2, 0, 1, 1)
        wildLifeDetector.setCentralWidget(self.centralwidget)

        self.retranslateUi(wildLifeDetector)
        self.tabWidget.setCurrentIndex(0)
        QtCore.QMetaObject.connectSlotsByName(wildLifeDetector)

    def retranslateUi(self, wildLifeDetector):
        _translate = QtCore.QCoreApplication.translate
        wildLifeDetector.setWindowTitle(_translate("wildLifeDetector", "Wild Life Animal Detector"))
        self.label_3.setText(_translate("wildLifeDetector", "Select save location for analysed video"))
        self.analyseButton.setText(_translate("wildLifeDetector", "Analyse"))
        self.InputFileLineEditAnalysis.setPlaceholderText(_translate("wildLifeDetector", "/home/user/name/folder/video.mp4"))
        self.InputFileButtonAnalysis.setText(_translate("wildLifeDetector", "Browse"))
        self.checkBoxWhole.setText(_translate("wildLifeDetector", "Analyse whole video"))
        self.frameStart.setText(_translate("wildLifeDetector", "0"))
        self.frameStart.setPlaceholderText(_translate("wildLifeDetector", "1"))
        self.checkBoxSection.setText(_translate("wildLifeDetector", "Analyse from frame"))
        self.labelTo.setText(_translate("wildLifeDetector", " to "))
        self.frameEnd.setText(_translate("wildLifeDetector", "100"))
        self.frameEnd.setPlaceholderText(_translate("wildLifeDetector", "100"))
        self.outputFileLineEditAnalysis.setPlaceholderText(_translate("wildLifeDetector", "/home/user/name/folder/video_analysed.mp4"))
        self.outputFileButtonAnalysis.setText(_translate("wildLifeDetector", "Browse"))
        self.checkBoxWhale.setText(_translate("wildLifeDetector", "Whales"))
        self.checkBoxDolphin.setText(_translate("wildLifeDetector", "Dolphins"))
        self.checkBoxSwitch.setText(_translate("wildLifeDetector", "(Convert detected animal to animal)"))
        self.labelOr.setText(_translate("wildLifeDetector", "Or"))
        self.label.setText(_translate("wildLifeDetector", "Select video for analysis"))
        self.stopButton.setText(_translate("wildLifeDetector", "Stop Analysing Now"))
        self.labelFrames.setText(_translate("wildLifeDetector", "The selected video contains x frames"))
        self.label_4.setText(_translate("wildLifeDetector", "Select animals to be detected"))
        self.labelCompletionTime.setText(_translate("wildLifeDetector", "Estimated completion time: 1:04:32"))
        self.tabWidget.setTabText(self.tabWidget.indexOf(self.analyseTab), _translate("wildLifeDetector", "Analyse Video"))
        self.procVideoLabel.setText(_translate("wildLifeDetector", "Please Load a Video\n"
"In the Analyse Video Tab"))
        self.label_5.setText(_translate("wildLifeDetector", "Animal Statistics"))
        self.countBox.setText(_translate("wildLifeDetector", "Total Whales: 20\n"
"Total Dolphins: 25"))
        self.sizeBox.setText(_translate("wildLifeDetector", "1. Small Dolphin 0.867\n"
"20. Medium Dolphin 0.657"))
        self.SaveAsButton.setText(_translate("wildLifeDetector", "Save Video As"))
        self.label_8.setText(_translate("wildLifeDetector", "Detection Options:"))
        self.checkBoxVidWhale.setText(_translate("wildLifeDetector", "Show Whales"))
        self.checkBoxVidDolphin.setText(_translate("wildLifeDetector", "Show Dolphins"))
        self.checkBoxVidSwitch.setText(_translate("wildLifeDetector", "(Convert detected animal to animal)"))
        self.label_2.setText(_translate("wildLifeDetector", "Overlay Options:"))
        self.checkBoxOverlayName.setText(_translate("wildLifeDetector", "Animal Name"))
        self.checkBoxOverlayConfidence.setText(_translate("wildLifeDetector", "Confidence Levels"))
        self.checkBoxOverlaySize.setText(_translate("wildLifeDetector", "Animal Size"))
        self.checkBoxOverlayCount.setText(_translate("wildLifeDetector", "Animal Counts"))
        self.tabWidget.setTabText(self.tabWidget.indexOf(self.viewTab), _translate("wildLifeDetector", "View Video"))
        self.label_6.setText(_translate("wildLifeDetector", "wild life animal detector"))


if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    wildLifeDetector = QtWidgets.QMainWindow()
    ui = Ui_wildLifeDetector()
    ui.setupUi(wildLifeDetector)
    wildLifeDetector.show()
    sys.exit(app.exec_())

