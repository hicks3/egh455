from PyQt5.QtCore import QDir, Qt, QUrl, QTimer, QThread, QRect, QObject, pyqtSignal
# from PyQt5.QtMultimedia import QMediaContent, QMediaPlayer
# from PyQt5.QtMultimediaWidgets import QVideoWidget
from PyQt5.QtWidgets import (QApplication, QFileDialog, QHBoxLayout, QLabel,
        QPushButton, QSizePolicy, QSlider, QStyle, QVBoxLayout, QWidget)

from PyQt5.QtWidgets import QMainWindow,QWidget, QPushButton, QAction
from PyQt5.QtGui import QIcon, QPixmap, QImage

import detection
import cv2 as cv
import numpy as np
from timeit import default_timer as timer
import threading
import time

import sys

class procVid:
    def __init__(self, mainWindow, windowChange):

        # Play Status of Video
        self.playing = False

        # Configure the video widget
        self.ui = mainWindow.ui
        self.player = QLabel(self.ui.procVideoLabel)

        # Play Button
        self.ui.playButtonProc.setIcon(self.ui.playButtonProc.style().standardIcon(QStyle.SP_MediaPlay))
        self.ui.playButtonProc.clicked.connect(self.playButtonPressed)

        # Window resize event
        self.windowChange = windowChange

        # Frame skip buttons
        self.ui.frameBackward.setIcon(self.ui.frameBackward.style().standardIcon(QStyle.SP_MediaSeekBackward))
        self.ui.frameForward.setIcon(self.ui.frameForward.style().standardIcon(QStyle.SP_MediaSeekForward))
        self.ui.frameBackward.clicked.connect(self.backwardButtonPressed)
        self.ui.frameForward.clicked.connect(self.forwardButtonPressed)

        # Video slider 
        self.ui.vidSliderProc.sliderReleased.connect(self.ChangeFrame)

        # Hide switch animal check box
        self.ui.checkBoxVidSwitch.hide()

        # Animal check boxes
        self.ui.checkBoxVidWhale.clicked.connect(self.OnAnimalCheckBoxClicked)
        self.ui.checkBoxVidDolphin.clicked.connect(self.OnAnimalCheckBoxClicked)
        self.ui.checkBoxVidSwitch.clicked.connect(self.OnAnimalCheckBoxClicked)
        self.ui.checkBoxSwitch.hide()

        # Overlay check boxes
        self.ui.checkBoxOverlayName.clicked.connect(self.OnOverlayNameCheckBoxClicked)
        self.ui.checkBoxOverlayConfidence.clicked.connect(self.OnOverlayConfidenceCheckBoxClicked)
        self.ui.checkBoxOverlaySize.clicked.connect(self.OnOverlaySizeCheckBoxClicked)
        self.ui.checkBoxOverlayCount.clicked.connect(self.OnOverlayCountCheckBoxClicked)

        # Animal stats
        self.ui.countBox.setText("")
        self.ui.sizeBox.setText("")

        # Save as button
        self.ui.SaveAsButton.clicked.connect(self.SaveVideo)

        self.signals = self.Signals()


    def SaveVideo(self):
        
        filename, filter = QFileDialog.getSaveFileName(caption = "Save File", directory = "./", filter = "Videos (*.mp4)")

        if filename:
            # print(filename)

            # Stop video
            self.videoPlayer.playing = False
            self.ui.playButtonProc.setIcon(self.ui.playButtonProc.style().standardIcon(QStyle.SP_MediaPlay))

            # Disable play button
            self.ui.playButtonProc.setEnabled(False)

            # Let video finish
            time.sleep(50/1000.0)

            self.videoPlayer.vid.CreateOutputVideo(filename)
            self.videoPlayer.vid.SetFrame(self.videoPlayer.startFrame)
            playFlag = True

            while playFlag:

                frame = self.videoPlayer.vid.GetNextFrame()
                

                # Add Bounding Box
                if self.videoPlayer.boxes[self.videoPlayer.vid.frame] != None and self.videoPlayer.animal > -1:
                    # print(self.animal)
                    frame, sizeBox, countBox = detection.OverlayFrame(frame, self.videoPlayer.boxes[self.videoPlayer.vid.frame], animal = self.videoPlayer.animal, convert = self.videoPlayer.convert, layName = self.videoPlayer.layName, layConfidence = self.videoPlayer.layConfidence, laySize = self.videoPlayer.laySize, layCount = self.videoPlayer.layCount)
                else:
                    sizeBox = ''
                    countBox = ''

                if np.any(frame):
                    self.videoPlayer.vid.SaveFrame(frame)
                else:
                    break

                if self.videoPlayer.vid.frame > self.videoPlayer.endFrame:
                    playFlag = False

            # Close file
            self.videoPlayer.vid.Finish()

            # Enable play button
            self.ui.playButtonProc.setEnabled(True)
            self.videoPlayer.vid.SetFrame(self.videoPlayer.startFrame)
            self.ui.vidSliderProc.setValue(self.videoPlayer.vid.frame)


                



    def OnOverlayNameCheckBoxClicked(self):
        self.videoPlayer.layName = self.ui.checkBoxOverlayName.isChecked()
        # Update frame if video is not playing
        if not self.videoPlayer.playing:
            self.SetPosition(self.videoPlayer.vid.frame - 1)
            self.videoPlayer.ProcessFrame()

    def OnOverlayConfidenceCheckBoxClicked(self):
        self.videoPlayer.layConfidence = self.ui.checkBoxOverlayConfidence.isChecked()
        # Update frame if video is not playing
        if not self.videoPlayer.playing:
            self.SetPosition(self.videoPlayer.vid.frame - 1)
            self.videoPlayer.ProcessFrame()

    def OnOverlaySizeCheckBoxClicked(self):
        self.videoPlayer.laySize = self.ui.checkBoxOverlaySize.isChecked()
        # Update frame if video is not playing
        if not self.videoPlayer.playing:
            self.SetPosition(self.videoPlayer.vid.frame - 1)
            self.videoPlayer.ProcessFrame()

    def OnOverlayCountCheckBoxClicked(self):
        self.videoPlayer.layCount = self.ui.checkBoxOverlayCount.isChecked()
        # Update frame if video is not playing
        if not self.videoPlayer.playing:
            self.SetPosition(self.videoPlayer.vid.frame - 1)
            self.videoPlayer.ProcessFrame()




    def OnAnimalCheckBoxClicked(self):
        statusW = self.ui.checkBoxVidWhale.isChecked()
        statusD = self.ui.checkBoxVidDolphin.isChecked()
        statusS = self.ui.checkBoxVidSwitch.isChecked()

        if (not statusW and not statusD):
            self.ui.checkBoxVidSwitch.hide()
            self.videoPlayer.animal = -1
        elif (statusW and statusD):
            self.videoPlayer.animal = 2

            self.ui.checkBoxVidSwitch.hide()
        elif (statusW and not statusD):
            self.videoPlayer.animal = 0

            self.ui.checkBoxVidSwitch.setText('(Convert detected dolphins to whales)')
            self.ui.checkBoxVidSwitch.show()
        elif (not statusW and statusD):
            self.videoPlayer.animal = 1

            self.ui.checkBoxVidSwitch.setText('(Convert detected whales to dolphins)')
            self.ui.checkBoxVidSwitch.show()

        if (self.videoPlayer.animal != 2 and statusS):
            self.videoPlayer.convert = True

        # Update frame if video is not playing
        if not self.videoPlayer.playing:
            self.SetPosition(self.videoPlayer.vid.frame - 1)
            self.videoPlayer.ProcessFrame()
        

    def SetAnimalCheckBox(self):
        if self.videoPlayer.animal == 2:
            self.ui.checkBoxVidDolphin.setChecked(True)
            self.ui.checkBoxVidWhale.setChecked(True)
            self.ui.checkBoxVidSwitch.hide()
        elif self.videoPlayer.animal == 0:
            self.ui.checkBoxVidSwitch.setText('(Convert detected dolphins to whales)')
            self.ui.checkBoxVidDolphin.setChecked(False)
            self.ui.checkBoxVidWhale.setChecked(True)
        elif self.videoPlayer.animal == 1:
            self.ui.checkBoxVidSwitch.setText('(Convert detected whales to dolphins)')
            self.ui.checkBoxVidDolphin.setChecked(True)
            self.ui.checkBoxVidWhale.setChecked(False)
        elif self.videoPlayer.animal == -1:
            self.ui.checkBoxVidDolphin.setChecked(False)
            self.ui.checkBoxVidWhale.setChecked(False)

        if self.videoPlayer.convert:
            self.ui.checkBoxVidSwitch.setChecked(True)
        else:
            self.ui.checkBoxVidSwitch.setChecked(False)


    def EnableOverlay(self, enable):
        self.ui.checkBoxVidWhale.setEnabled(enable)
        self.ui.checkBoxVidDolphin.setEnabled(enable)
        self.ui.checkBoxVidSwitch.setEnabled(enable)
        self.ui.checkBoxOverlayConfidence.setEnabled(enable)
        self.ui.checkBoxOverlayName.setEnabled(enable)
        self.ui.checkBoxOverlaySize.setEnabled(enable)
        self.ui.checkBoxOverlayCount.setEnabled(enable)
        self.ui.SaveAsButton.setEnabled(enable)


    def InitPlayer(self, input):

        self.videoPlayer = self.VideoPlayer(input, self.ui, self.windowChange, self.signals)
        self.videoPlayer.OpenVid()
        self.videoPlayer.ProcessFrame()
        self.EnableOverlay(False)
        self.videoPlayer.startFrame = 0
        self.videoPlayer.endFrame = self.videoPlayer.vid.frameCount

        # Animal stats
        self.ui.countBox.setText("")
        self.ui.sizeBox.setText("")



    def UpdateModel(self, boxes = None, animal = 2, convert = False, frameStart = 0, frameEnd = 1):
        
        self.videoPlayer.OpenVid()
        self.videoPlayer.Update(boxes, animal, convert, frameStart, frameEnd)

        self.SetPosition(frameStart)
        self.videoPlayer.ProcessFrame()
        self.EnableOverlay(True)
        self.SetAnimalCheckBox()


    def playButtonPressed(self):

        if (self.videoPlayer.playing):
            self.videoPlayer.playing = False
            self.ui.playButtonProc.setIcon(self.ui.playButtonProc.style().standardIcon(QStyle.SP_MediaPlay))
        else:
            self.videoPlayer.playing = True
            self.ui.playButtonProc.setIcon(self.ui.playButtonProc.style().standardIcon(QStyle.SP_MediaPause))

            self.videoPlayer.start()


    def backwardButtonPressed(self):

        if not self.videoPlayer.playing:
            self.SetPosition(self.videoPlayer.vid.frame - 2)
            self.videoPlayer.ProcessFrame()

    def forwardButtonPressed(self):
        if not self.videoPlayer.playing:
            self.SetPosition(self.videoPlayer.vid.frame)
            self.videoPlayer.ProcessFrame()

    def ChangeFrame(self):
        if not self.videoPlayer.playing:
            self.videoPlayer.vid.SetFrame(self.ui.vidSliderProc.value()-1)
            self.videoPlayer.ProcessFrame()

    def SetPosition(self, position):
       self.videoPlayer.vid.SetFrame(position)


    class VideoPlayer(QThread):

        def __init__(self, videoPath, ui, windowChange, signals):
            QThread.__init__(self)

            self.path = videoPath
            self.playing = False
            self.vid = None
            self.ui = ui
            self.windowChange = windowChange
            self.sizeLock = threading.Lock()

            self.boxes = None
            self.animal = 2
            self.threshold = 0.5
            self.convert = False

            # Overlay options
            self.layName = True
            self.layConfidence = True
            self.laySize = False
            self.layCount = False

            # Video player size
            self.currentSize = self.ui.vidFrame.size()
            self.lastSize = self.ui.vidFrame.size()
            self.defaultSize = self.ui.vidFrame.size()
            self.sameCount = 0

            # Signals
            self.signals = signals
            self.signals.draw.connect(self.DrawImage)
            self.signals.progressUpdate.connect(self.UpdateProgressBar)

            # Frame to play
            self.startFrame = 0
            self.endFrame = 1


        def Update(self, boxes, animal, convert, startFrame, endFrame):
            self.boxes = boxes
            self.animal = animal
            self.threshold = 0.5
            self.convert = convert
            self.startFrame = startFrame
            self.endFrame = endFrame
            self.ui.vidSliderProc.setMaximum(endFrame + 1)
            self.ui.vidSliderProc.setMinimum(startFrame + 1)

        # QThread override
        def __del__(self):
            self.wait()


        # QThread overide, section which runs in a parallel thread
        def run(self):
            self.PlayVideo()
            return

        # def WindowChanged(self):
        #     self.sizeLock.acquire()
        #     try:
        #         # self.lastFrameSize = self.frameSize
        #         self.frameSize = self.ui.vidFrame.size()
        #         # self.frameCount +=1
        #     finally:
        #         self.sizeLock.release()


        # Opens a video at 'path'
        def OpenVid(self):
            if self.vid is not None:
                if self.vid.isOpen:
                    self.CloseVid()
            self.vid = detection.VideoControl(self.path)

            self.ui.vidSliderProc.setMaximum(self.vid.frameCount)
            self.ui.vidSliderProc.setMinimum(1)

            if self.boxes is None:
                self.boxes = [None] * (int(self.vid.frameCount) + 1)


        # Releases the video
        def CloseVid(self):
            self.vid.Close()


        # Plays the video while frames are avaliable and playing is True
        def PlayVideo(self):        

            playFlag = True

            while playFlag and self.playing:
                
                start = timer()

                if self.vid.frame > self.endFrame:
                    self.vid.SetFrame(self.startFrame)
                    self.ui.vidSliderProc.setValue(self.startFrame + 1)
                    return

                frame = self.vid.GetNextFrame()



                # self.ui.vidSliderProc.setValue(self.vid.frame)
                self.signals.progressUpdate.emit(self.vid.frame)

                # Add Bounding Box
                if self.boxes[self.vid.frame] != None and self.animal > -1:
                    # print(self.animal)
                    frame, sizeBox, countBox = detection.OverlayFrame(frame, self.boxes[self.vid.frame], animal = self.animal, convert = self.convert, layName = self.layName, layConfidence = self.layConfidence, laySize = self.laySize, layCount = self.layCount)
                else:
                    sizeBox = ''
                    countBox = ''

                if np.any(frame):
                    qFrame = self.ConvertImage(frame)
                    playFlag = self.vid.playFlag
                    pix = QPixmap(qFrame)
                    pix = self.RescaleImage(pix)

                    self.nextFrame = pix
                    self.nextSize = sizeBox
                    self.nextCount = countBox

                    self.signals.draw.emit()
                    
                else:
                    playFlag = False

                if self.vid.frame > self.endFrame:
                    playFlag = False
                    

                stop = timer()
                ms = (stop-start) * 1000
                FPS_ms = 1000/self.vid.fps

                if ms > FPS_ms:
                    delay = 1
                else:
                    if (int(FPS_ms - ms)) > 0:
                        delay = int(FPS_ms - ms)
                    else:
                        delay = 1

                time.sleep(delay/1000.0)

            self.ui.playButtonProc.setIcon(self.ui.playButtonProc.style().standardIcon(QStyle.SP_MediaPlay))
            self.playing = False

            if (self.vid.frame >= self.vid.frameCount or self.vid.frame >= self.endFrame):
                self.vid.playFlag = True
                # self.vid.SetFrame(0)
                self.vid.SetFrame(self.startFrame)
                self.ui.vidSliderProc.setValue(self.startFrame + 1)


        # Process a single frame for viewing
        def ProcessFrame(self):

            if self.vid.frame > self.endFrame:
                self.vid.SetFrame(self.endFrame+1)
                return
            elif self.vid.frame < self.startFrame:
                self.vid.SetFrame(self.startFrame+1)
                return

            frame = self.vid.GetNextFrame()

            self.ui.vidSliderProc.setValue(self.vid.frame)

            # Add Bounding Box
            if self.boxes[self.vid.frame] != None and self.animal > -1:
                frame, sizeBox, countBox = detection.OverlayFrame(frame, self.boxes[self.vid.frame], animal = self.animal, convert = self.convert, layName = self.layName, layConfidence = self.layConfidence, laySize = self.laySize, layCount = self.layCount)
            else:
                countBox = ""
                sizeBox = ""

            if np.any(frame):
                qFrame = self.ConvertImage(frame)
                pix = QPixmap(qFrame)
                pix = self.RescaleImage(pix)
                
                self.ui.procVideoLabel.setPixmap(pix)
                self.ui.procVideoLabel.show()
                self.ui.procVideoLabel.adjustSize()
                self.ui.countBox.setText(countBox)
                self.ui.sizeBox.setText(sizeBox)
                # self.ui.procVideoLabel.setAlignment(Qt.AlignCenter)

            # if self.vid.frame > self.endFrame:
            #     self.vid.SetFrame(self.endFrame)
            # elif self.vid.frame < self.startFrame:
            #     self.vid.SetFrame(self.startFrame)
                # self.vid.SetFrame(max(self.startFrame-1, 0))


        # Converts an image to QT format
        def ConvertImage(self, image):
            height, width, channel = image.shape
            bytesPerLine = 3 * width
            image = cv.cvtColor(image, cv.COLOR_BGR2RGB)
            qImage = QImage(image.data, width, height, bytesPerLine, QImage.Format_RGB888)
            return qImage
        

        # Rescales image to take up avaliable spave in the video frame
        # Rescales the label to be the size if the image
        def RescaleImage(self, pix):

            #  = self.ui.vidFrame.size()
            # w = self.ui.vidFrame.width() - 100
            # h = self.ui.vidFrame.height()
            # sizeIm.width = sizeIm.width - 100

            # pix = pix.scaled( QSize(w, h), Qt.KeepAspectRatio, Qt.SmoothTransformation)
            pix = pix.scaled( self.ui.vidFrame.size(), Qt.KeepAspectRatio, Qt.SmoothTransformation)
            return pix

        def DrawImage(self):
            self.ui.procVideoLabel.setPixmap(self.nextFrame)
            self.ui.procVideoLabel.show()
            self.ui.procVideoLabel.adjustSize()
            # self.ui.procVideoLabel.setAlignment(Qt.AlignCenter)

            self.ui.countBox.setText(self.nextCount)
            self.ui.sizeBox.setText(self.nextSize)

        def UpdateProgressBar(self, value):
            self.ui.vidSliderProc.setValue(value)


    # Signals for video player thread to gui thread
    class Signals(QObject):
        draw = pyqtSignal()
        progressUpdate = pyqtSignal(int)

