from PyQt5.QtCore import QDir, Qt, QUrl, QTimer, QThread, QObject, pyqtSignal
# from PyQt5.QtMultimedia import QMediaContent, QMediaPlayer
# from PyQt5.QtMultimediaWidgets import QVideoWidget
from PyQt5.QtWidgets import (QApplication, QFileDialog, QHBoxLayout, QLabel,
        QPushButton, QSizePolicy, QSlider, QStyle, QVBoxLayout, QWidget, QMessageBox)
from PyQt5.QtWidgets import QMainWindow,QWidget, QPushButton, QAction
from PyQt5.QtGui import QIcon, QIntValidator
from wildlife_detector_window import Ui_wildLifeDetector
import sys

# from orig_vid import origVid
from procVid import procVid

import detection
import math
import threading
import time

import cv2 as cv


class analyseVid:
    def __init__(self, mainWindow, procVid):
        self.ui = mainWindow.ui
        self.viewTab = self.ui.tabWidget

        # Hide Sections
        self.DisplayLabels(hide = True)

        # File selection button
        self.ui.InputFileButtonAnalysis.clicked.connect(self.onInputFileButtonClicked)
        self.ui.outputFileButtonAnalysis.clicked.connect(self.onOutputFileButtonClicked)

        # Check boxes
        self.ui.checkBoxWhale.clicked.connect(self.onCheckBoxClicked)
        self.ui.checkBoxDolphin.clicked.connect(self.onCheckBoxClicked)
        self.ui.checkBoxSwitch.clicked.connect(self.onCheckBoxClicked)
        self.ui.checkBoxSwitch.hide()
        self.animals = 2

        # Frame option checkboxes
        self.ui.checkBoxWhole.clicked.connect(self.onCheckBoxFrameClicked)
        self.ui.checkBoxSection.clicked.connect(self.onCheckBoxFrameClicked)
        self.videoSection = False

        # Frame selection inputs
        self.ui.frameStart.textChanged.connect(self.onFrameSelectionChanged)
        self.ui.frameEnd.textChanged.connect(self.onFrameSelectionChanged)

        # Analyse Video Button
        self.ui.analyseButton.clicked.connect(self.analyseButtonPressed)
        self.ui.progressBar.hide()

        # Create signals object
        self.signals = self.Signals()
        self.signals.finished.connect(self.ResetGui)

        # Hide stop button
        self.ui.stopButton.clicked.connect(self.StopButtonPressed)
        self.ui.stopButton.hide()

        # Disable view tab
        self.ui.viewTab.setEnabled(False)

        # Start view video process
        self.procVid = procVid



    def onFrameSelectionChanged(self):

        try:
            start = int(self.ui.frameStart.text())
        except ValueError:
            start = 0

        try:
            end = int(self.ui.frameEnd.text())
        except ValueError:
            end = int(self.video.frameCount)

        startValid = QIntValidator(0, end-1)
        # startValid.fixup('1')
        
        endValid = QIntValidator(start+1, int(self.video.frameCount)-1)

        # print(self.ui.frameStart.hasAcceptableInput ())
        # print(self.ui.frameEnd.hasAcceptableInput ())
        
        # endValid.fixup(str(int(self.video.frameCount)))

        self.ui.frameStart.setValidator(startValid)
        self.ui.frameEnd.setValidator(endValid)

        if self.videoSection:
            self.CalculateTime(end-start + 1)

    def onCheckBoxFrameClicked(self):

        self.videoSection = self.ui.checkBoxSection.isChecked()#not(self.videoSection)

        if self.videoSection:
            try:
                start = int(self.ui.frameStart.text())
            except ValueError:
                start = 1

            try:
                end = int(self.ui.frameEnd.text())
            except ValueError:
                end = int(self.video.frameCount)


            self.CalculateTime(end-start + 1)
        else:
            self.CalculateTime()


    def onCheckBoxClicked(self):
        statusW = self.ui.checkBoxWhale.isChecked()
        statusD = self.ui.checkBoxDolphin.isChecked()
        statusS = self.ui.checkBoxSwitch.isChecked()

        if (not statusW and not statusD):
            self.ui.checkBoxSwitch.hide()
            self.animals = -1
        elif (statusW and statusD):
            self.animals = 2

            self.ui.checkBoxSwitch.hide()
        elif (statusW and not statusD):
            self.animals = 0

            self.ui.checkBoxSwitch.setText('(Convert detected dolphins to whales)')
            self.ui.checkBoxSwitch.show()
        elif (not statusW and statusD):
            self.animals = 1

            self.ui.checkBoxSwitch.setText('(Convert detected whales to dolphins)')
            self.ui.checkBoxSwitch.show()

        if (self.animals != 2 and statusS):
            self.convert = True
        
        self.EnableAnalyseButton()


    def onInputFileButtonClicked(self):
       filename, filter = QFileDialog.getOpenFileName(self.viewTab, "Open File","./", "Videos (*.mp4 *.avi)")

       if filename:

            if self.CheckVideo(filename):

                self.ShowVideoStats(filename)

                # Open video in view video tab
                self.procVid.InitPlayer(filename)
                self.ui.viewTab.setEnabled(True)

                outPath = filename.split('.')
                fileNameOut = outPath[0] + '_analysed.mp4'

                self.ui.InputFileLineEditAnalysis.setText(filename)
                self.ui.outputFileLineEditAnalysis.setText(fileNameOut)

                self.EnableAnalyseButton()

        #    if (self.animals >= 0):
        #     self.ui.analyseButton.setEnabled(True)


    def DisplayLabels(self, hide = True):
        if hide:
            # Hide sections
            self.ui.labelFrames.hide()
            self.ui.checkBoxWhole.hide()
            self.ui.labelOr.hide()
            self.ui.checkBoxSection.hide()
            self.ui.frameStart.hide()
            self.ui.labelTo.hide()
            self.ui.frameEnd.hide()
            self.ui.labelCompletionTime.hide()
        else:
            # Display sections
            self.ui.labelFrames.show()
            self.ui.checkBoxWhole.show()
            self.ui.labelOr.show()
            self.ui.checkBoxSection.show()
            self.ui.frameStart.show()
            self.ui.labelTo.show()
            self.ui.frameEnd.show()
            self.ui.labelCompletionTime.show()

    # Check that selected video is valid and works
    def CheckVideo(self, filename):
        self.video = detection.VideoControl(filename)

        if self.video.valid:
            return True
        else:
            return False

    def ShowVideoStats(self, filename):
        # Get video stats
        self.ui.labelFrames.setText('The selected video contains ' + str(int(self.video.frameCount)) + ' frames')
        self.ui.frameEnd.setText(str(int(self.video.frameCount)-1))
        self.ui.frameStart.setText(str(0))
        self.CalculateTime()
        self.DisplayLabels(hide = False)
        self.onFrameSelectionChanged()
        


    def CalculateTime(self, frameCount = 0):

        if frameCount == 0:
            frames = self.video.frameCount
        else:
            frames = frameCount

        totalTime = 4.8 * frames + 20
        self.currentTime = totalTime

        hours = int(math.floor(totalTime / 3600))
        totalTime -= (hours * 3600)
        minutes = int(math.floor(totalTime / 60))
        totalTime -= (minutes * 60)
        totalTime = int(round(totalTime))

        if hours > 0:
            timeStr = '{:d}:{:02d}:{:02d}'.format(hours, minutes, totalTime)
        else:
            timeStr = '{:d}:{:02d}'.format(minutes, totalTime)
        
        self.ui.labelCompletionTime.setText('Estimated completion time: ' + timeStr)


    def UpdateTime(self, newTime = -2):

        if newTime == -2:
            self.currentTime -= 1
            self.currentTime = max(0, self.currentTime)
        else:
            self.currentTime = newTime

        totalTime = self.currentTime
        hours = int(math.floor(totalTime / 3600))
        totalTime -= (hours * 3600)
        minutes = int(math.floor(totalTime / 60))
        totalTime -= (minutes * 60)
        totalTime = int(round(totalTime))

        if hours > 0:
            timeStr = '{:d}:{:02d}:{:02d}'.format(hours, minutes, totalTime)
        else:
            timeStr = '{:d}:{:02d}'.format(minutes, totalTime)
        
        self.ui.labelCompletionTime.setText('Estimated completion time: ' + timeStr)



    def EnableAnalyseButton(self):
        if self.animals >= 0 and self.ui.InputFileLineEditAnalysis.text() and self.ui.outputFileLineEditAnalysis.text():
            self.ui.analyseButton.setEnabled(True)
        else:
            self.ui.analyseButton.setEnabled(False)


    def onOutputFileButtonClicked(self):
       filename, filter = QFileDialog.getSaveFileName(self.viewTab, "Save File","./", "Videos (*.mp4)")

       if filename:
           self.ui.outputFileLineEditAnalysis.setText(filename)

    def classDetectionChange(self,ui):
       classSelection = self.ui.animalSelectionBox.currentText()
       print(classSelection)

    def disableInputs(self, enable = False):
        self.ui.outputFileLineEditAnalysis.setEnabled(enable)
        self.ui.checkBoxWhale.setEnabled(enable)
        self.ui.checkBoxDolphin.setEnabled(enable)
        self.ui.checkBoxSwitch.setEnabled(enable)
        self.ui.checkBoxWhole.setEnabled(enable)
        self.ui.checkBoxSection.setEnabled(enable)
        self.ui.frameStart.setEnabled(enable)
        self.ui.frameEnd.setEnabled(enable)
        self.ui.InputFileButtonAnalysis.setEnabled(enable)
        self.ui.outputFileButtonAnalysis.setEnabled(enable)



    class AnalyseVideo():

        def __init__(self, ui, signals, updateTimeFunc, guiResetFunc, frameTotal, boxes, inputfile, outputFile, frameStart, frameEnd, animals, convert):
            self.ui = ui
            self.signals = signals
            self.time = [0, 0, 0, 0, 0]
            self.timeI = 0
            self.full = False
            self.updateTimeFunc = updateTimeFunc
            self.guiResetFunc = guiResetFunc
            self.frameTotal = frameTotal
            self.inputfile = inputfile
            self.boxes = boxes
            self.outputFile = outputFile
            self.frameStart = frameStart
            self.frameEnd = frameEnd
            self.animals = animals
            self.convert = convert

            self.sess = detection.get_session()

            self.stopFlag = [0]
            self.signals.stop.connect(self.StopVideo)

        def StopVideo(self):
            self.stopFlag[0] = 1

        def StartNet(self):

            # Open video
            video = detection.VideoControl(self.inputfile)

            # Load neural net model
            with self.sess.graph.as_default():
                detection.LoadModel('resnet50_best.h5')

            # Start
            detection.DetectVideo(video, self.boxes, func = self.UpdateUI, output = self.outputFile, save = True, frameStart = self.frameStart, frameEnd = self.frameEnd, animal = self.animals, convert = self.convert, stopFlag = self.stopFlag)
            
            if video.frame == video.frameCount:
                finalFrame = video.frame - 1
            else:
                finalFrame = video.frame - 2

            # print(finalFrame)
            # print(self.frameEnd)

            video.Close()

            # Emit signal
            self.signals.finished.emit(finalFrame)

        # def UpdateUI(self, thr, progressBar):
        def UpdateUI(self, count, time):

            if self.stopFlag[0] == 0:
                self.ui.progressBar.setValue(count)
                remainingFrames = self.frameTotal - count
                self.time[self.timeI] = time 
                self.timeI += 1

                if self.timeI == 5:
                    self.full = True
                
                self.timeI = self.timeI % 5
                tTotal = 0

                if self.full:
                    rangeI = 5
                else:
                    rangeI = self.timeI
                
                for i in range(rangeI):
                    tTotal += self.time[i]

                tTotal /= rangeI
                self.updateTimeFunc(tTotal * remainingFrames)

        def UpdateTime(self, thr, func):
            while (thr.isAlive() and self.stopFlag[0] == 0):
                func()
                time.sleep(1)

            # self.progressBar.setValue(self.frameTotal)
            self.ui.progressBar.setValue(self.frameTotal)

            if self.stopFlag[0] == 0:
                self.updateTimeFunc(0)
            # self.guiResetFunc(self.inputfile, self.boxes)




    def analyseButtonPressed(self):
        self.input = self.ui.InputFileLineEditAnalysis.text()
        self.output = self.ui.outputFileLineEditAnalysis.text()

        if self.input and self.output and self.animals >= 0:
            
            # Disable form controls
            self.disableInputs()

            # Display stop button
            self.ui.stopButton.show()
            self.ui.stopButton.setEnabled(True)

            # Obtain usefull information
            self.convert = self.ui.checkBoxSwitch.isChecked()

            # Get users desired processed frames
            if self.videoSection:
                try:
                    self.frameStart = int(self.ui.frameStart.text())
                except ValueError:
                    self.frameStart = 0

                try:
                    self.frameEnd = int(self.ui.frameEnd.text())
                except ValueError:
                    self.frameEnd = int(self.video.frameCount)-1
            else:
                self.frameStart = 0
                self.frameEnd = int(self.video.frameCount)-1

            if not self.ui.frameStart.hasAcceptableInput():
                self.frameStart = 0
                self.ui.frameStart.setText(str(self.frameStart))

            if not self.ui.frameEnd.hasAcceptableInput():
                self.frameEnd = int(self.video.frameCount)-1
                self.ui.frameEnd.setText(str(self.frameEnd))


            frameTotal = self.frameEnd - (self.frameStart - 1)

            self.boxes = [None] * (int(self.video.frameCount) + 1)

                        # Close video file
            self.video.Close()


            #show progress bar and set initial value to 0%
            self.ui.progressBar.show()
            #   self.ui.progressLabel.show()
            self.ui.progressBar.setValue(0)
            self.ui.progressBar.setMaximum(frameTotal)
            self.ui.analyseButton.hide()

            # Stop video in other tab
            self.procVid.videoPlayer.playing = False
            self.procVid.videoPlayer.CloseVid()

            # Disbale other tab
            self.ui.viewTab.setEnabled(False)


            # Detect!
            analist = self.AnalyseVideo(self.ui, self.signals, self.UpdateTime, self.ResetGui, frameTotal, self.boxes, self.input, self.output, self.frameStart, self.frameEnd, self.animals, self.convert)
            # analist.start()
            # sess = detection.get_session()
            # thrVid = threading.Thread(target=analist.StartNet, kwargs = {'boxes': self.boxes, 'sess': sess, 'outputFile' : self.output, 'save': True, 'frameStart' : frameStart, 'frameEnd' : frameEnd, 'animal' : self.animals, 'convert' : convert} )
            # thrVid.start()

            thrVid = threading.Thread(target=analist.StartNet)
            thrVid.start()

            # Thread for countdown
            thrTime = threading.Thread(target=analist.UpdateTime, args=(thrVid, self.UpdateTime))
            thrTime.start()

        else:
            QMessageBox.about(self.viewTab, "Error", "Check that you have completed the form correctly")

    def ResetGui(self, finalFrame):
        self.disableInputs(enable = True)
        # self.ui.tabWidget.addTab(self.viewTab, 'View Video')
        self.ui.viewTab.setEnabled(True)
        self.ui.tabWidget.setCurrentIndex(1)
        self.DisplayLabels(True)
        self.ui.progressBar.hide()
        self.ui.analyseButton.show()
        self.ui.stopButton.hide()

        self.ui.InputFileLineEditAnalysis.setText('')
        self.ui.outputFileLineEditAnalysis.setText('')
        self.EnableAnalyseButton()

        self.procVid.UpdateModel(boxes = self.boxes, animal = self.animals, convert = self.convert, frameStart = self.frameStart, frameEnd = finalFrame)

        # self.procVid.InitPlayer(input, boxes)

    def TabChanged(self):
        self.procVid.InitPlayer(self.input)


    # Signals for video player thread to gui thread
    class Signals(QObject):
        finished = pyqtSignal(int)
        stop = pyqtSignal()
        # progressUpdate = pyqtSignal(int)

    def StopButtonPressed(self):
        self.signals.stop.emit()
        self.ui.labelCompletionTime.setText('Stopping Operation')
        self.ui.stopButton.setEnabled(False)