import sys
from PyQt5.QtCore import QDir, Qt, QUrl
# from PyQt5.QtMultimedia import QMediaContent, QMediaPlayer
# from PyQt5.QtMultimediaWidgets import QVideoWidget
from PyQt5.QtWidgets import (QApplication, QFileDialog, QHBoxLayout, QLabel,
        QPushButton, QSizePolicy, QSlider, QStyle, QVBoxLayout, QWidget)
from PyQt5.QtWidgets import QMainWindow,QWidget, QPushButton, QAction
from PyQt5.QtGui import QIcon


class origVid:

    def __init__(self, mainWindow):
        # Configure the video widget
        self.ui = mainWindow.ui

        self.videoPlayerOrig = QMediaPlayer(None, QMediaPlayer.VideoSurface)

        self.videoPlayerOrig.positionChanged.connect(self.positionChanged)
        self.videoPlayerOrig.durationChanged.connect(self.durationChanged)

        #Play Button
        self.ui.playButtonOrig.setIcon(self.ui.playButtonOrig.style().standardIcon(QStyle.SP_MediaPlay))
        self.ui.playButtonOrig.clicked.connect(self.playButtonPressed)

        #Pause Button
        self.ui.pauseButtonOrig.setIcon(self.ui.pauseButtonOrig.style().standardIcon(QStyle.SP_MediaPause))
        self.ui.pauseButtonOrig.clicked.connect(self.pauseButtonPressed)

        #Stop Button
        self.ui.stopButtonOrig.setIcon(self.ui.stopButtonOrig.style().standardIcon(QStyle.SP_MediaStop))
        self.ui.stopButtonOrig.clicked.connect(self.stopButtonPressed)

        #Slider
        self.ui.vidSliderOrig.setRange(0, 0)
        self.ui.vidSliderOrig.sliderMoved.connect(self.setPosition)


    def playButtonPressed(self):
       # Start video playback
       self.videoPlayerOrig.play()

    def pauseButtonPressed(self):
       # Pause video playback
       self.videoPlayerOrig.pause()

    def stopButtonPressed(self):
       # Stop video playback
       self.videoPlayerOrig.stop()

    def setPosition(self, position):
       self.videoPlayerOrig.setPosition(position)

    def positionChanged(self, position):
      self.ui.vidSliderOrig.setValue(position)

    def durationChanged(self, duration):
       self.ui.vidSliderOrig.setRange(0, duration)

