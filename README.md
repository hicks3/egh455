
These instructions on how to set up environment for GUI

- Use Python 3.6

* Install PyQt

	conda install pyqt

* Install QT Creator Suite

	Follow this link to download the QT Creator Suite(https://www1.qt.io/offline-installers/?hsLang=en#section-11) for your OS. 



* Running GUI

	python -m PyQt5.uic.pyuic -x wildlife_detector_window.ui -o wildlife_detector_window.py

	python wildlife_detector.py