#!/usr/bin/env python
# -*- coding: ascii -*-

"""
detection
~~~~~~~~~~~~~

A description which can be long and explain the complete
functionality of this module even with indented code examples.
Class/Function however should not be documented here.

"""



""" ------------ Imports ------------ """

# Context manager for redirecting stdout
import contextlib

with contextlib.redirect_stdout(None):

    # Import keras
    import keras

    # Import keras_retinanet
    from keras_retinanet import models
    from keras_retinanet.utils.image import read_image_bgr, preprocess_image, resize_image
    from keras_retinanet.utils.visualization import draw_box, draw_caption
    from keras_retinanet.utils.colors import label_color

    # Import miscellaneous modules
    # import matplotlib.pyplot as plt
    import cv2 as cv
    import os
    import numpy as np
    import time
    import asyncio
    import math

    # Set tf backend to allow memory to grow, instead of claiming everything
    import tensorflow as tf

    def get_session():
        config = tf.ConfigProto()
        config.gpu_options.allow_growth = True
        return tf.Session(config=config)

    # Use this environment flag to change which GPU to use
    #os.environ["CUDA_VISIBLE_DEVICES"] = "1"

    # Set the modified tf session as backend in keras
    keras.backend.tensorflow_backend.set_session(get_session())



""" ------------ Argument Parsing ------------ """

def args():
    # ap = argparse.ArgumentParser()
    # ap.add_argument("-s", "--speed", required = False, default = 30,
    #                 help = "Set the FPS of the played video")
    # ap.add_argument("-f", "--frame", required = False, default = 0, 
    #                 help = "Specify the frame number to start at"),
    # ap.add_argument("-t", "--time", required = False, default = 0, 
    #             help = "Specify the time (in ms) to start video at"),
    # args = vars(ap.parse_args())
    return args






""" ------------ Load RetinaNet model ------------ """

def LoadModel(modelName):
    # adjust this to point to your downloaded/trained model
    # models can be downloaded here: https://github.com/fizyr/keras-retinanet/releases
    # model_path = os.path.join('..', 'snapshots', 'resnet50_coco_best_v2.1.0.h5')
    # model_path = os.path.join('..', 'snapshots', modelName)
    # model_path = 'C:\\dev\\keras-retinanet\\snapshots\\' + modelName
    model_path = '../keras-retinanet/snapshots/' + modelName

    # print(model_path)

    # load retinanet model
    global model
    # model = models.load_model(model_path, backbone_name='resnet50')

    # if the model is not converted to an inference model, use the line below
    # see: https://github.com/fizyr/keras-retinanet#converting-a-training-model-to-inference-model
    model = models.load_model(model_path, backbone_name='resnet50', convert=True)

    model._make_predict_function()
    # model._make_test_function()
    # model._make_train_function()

    #print(model.summary())

    # load label to names mapping for visualization purposes
    global labels_to_names
    # labels_to_names = {0: 'person', 1: 'bicycle', 2: 'car', 3: 'motorcycle', 4: 'airplane', 5: 'bus', 6: 'train', 7: 'truck', 8: 'boat', 9: 'traffic light', 10: 'fire hydrant', 11: 'stop sign', 12: 'parking meter', 13: 'bench', 14: 'bird', 15: 'cat', 16: 'dog', 17: 'horse', 18: 'sheep', 19: 'cow', 20: 'elephant', 21: 'bear', 22: 'zebra', 23: 'giraffe', 24: 'backpack', 25: 'umbrella', 26: 'handbag', 27: 'tie', 28: 'suitcase', 29: 'frisbee', 30: 'skis', 31: 'snowboard', 32: 'sports ball', 33: 'kite', 34: 'baseball bat', 35: 'baseball glove', 36: 'skateboard', 37: 'surfboard', 38: 'tennis racket', 39: 'bottle', 40: 'wine glass', 41: 'cup', 42: 'fork', 43: 'knife', 44: 'spoon', 45: 'bowl', 46: 'banana', 47: 'apple', 48: 'sandwich', 49: 'orange', 50: 'broccoli', 51: 'carrot', 52: 'hot dog', 53: 'pizza', 54: 'donut', 55: 'cake', 56: 'chair', 57: 'couch', 58: 'potted plant', 59: 'bed', 60: 'dining table', 61: 'toilet', 62: 'tv', 63: 'laptop', 64: 'mouse', 65: 'remote', 66: 'keyboard', 67: 'cell phone', 68: 'microwave', 69: 'oven', 70: 'toaster', 71: 'sink', 72: 'refrigerator', 73: 'book', 74: 'clock', 75: 'vase', 76: 'scissors', 77: 'teddy bear', 78: 'hair drier', 79: 'toothbrush'}
    labels_to_names = {0: 'whale', 1: 'dolphin'}



""" ------------ Infer Image ------------ """

def InferImage(image, frameNum):
    # load image
    # image = read_image_bgr('000000008021.jpg')
    # image = read_image_bgr('dolphin_0.jpg')

    # copy to draw on
    draw = image.copy()
    draw = cv.cvtColor(draw, cv.COLOR_BGR2RGB)

    # preprocess image for network
    image = preprocess_image(image)
    image, scale = resize_image(image)

    # process image
    start = time.time()
    boxes, scores, labels = model.predict_on_batch(np.expand_dims(image, axis=0))

    
    print('\nFrame {}'.format(frameNum))
    print("Processing time: {:.2f}".format(time.time() - start))

    # correct for image scale
    boxes /= scale

    # Make bounding box object
    bBox = BoundingBox(frameNum, boxes, scores, labels)

    # # visualize detections
    # for box, score, label in zip(boxes[0], scores[0], labels[0]):
    #     # scores are sorted so we can break
    #     if score < 0.5:
    #         break
            
    #     color = label_color(label)
        
    #     b = box.astype(int)
    #     draw_box(draw, b, color=color)
        
    #     caption = "{} {:.3f}".format(labels_to_names[label], score)
    #     draw_caption(draw, b, caption)
    
    return bBox, (time.time() - start)



""" ------------ Bounding Box Class ------------ """

class BoundingBox:

    def __init__(self, frameNum, box, score, label):

        self.frameNum = frameNum
        self.box = box
        self.score = score
        self.label = label






""" ------------ Video Control Class ------------ """

class VideoControl:

    playFlag = True

    def __init__(self, videoPath):
        # cap = cv.VideoCapture('dataset\\video\\Sunflower_demo.mp4')
        self.videoPath = videoPath
        self.cap = cv.VideoCapture(videoPath)

        if self.cap.isOpened():
            self.fps = self.cap.get(cv.CAP_PROP_FPS)
            self.frame = int(self.cap.get(cv.CAP_PROP_POS_FRAMES))
            self.frameCount = self.cap.get(cv.CAP_PROP_FRAME_COUNT)
            self.length = self.frameCount / self.fps
            self.frameSize = (int(self.cap.get(cv.CAP_PROP_FRAME_WIDTH)), int(self.cap.get(cv.CAP_PROP_FRAME_HEIGHT)))
            self.isOpen = True
            self.valid = True
        else:
            self.valid = False

    def PrintVideoStats(self):
        print('\nVideo Statistics:')
        print('Path: %s' % self.videoPath)
        print('Length: {:.2f} seconds'.format(self.length))
        print('Frame Rate: %s fps' % self.fps)
        print('Frame Size: x:%s, y:%s pixels' % (int(self.frameSize[0]), int(self.frameSize[1]) ))
        print('Total Frames: %s' % int(self.frameCount))

    def GetFrame(self, frameNumber):
        self.cap.set(cv.CAP_PROP_POS_FRAMES, frameNumber)
        self.playFlag, image = self.cap.read()
        self.frame = int(self.cap.get(cv.CAP_PROP_POS_FRAMES))

        if self.playFlag:
            return image

    def GetNextFrame(self):
        self.playFlag, image = self.cap.read()
        self.frame = int(self.cap.get(cv.CAP_PROP_POS_FRAMES))

        if self.playFlag:
            return image

    def SetFrame(self, frameNumber):
        self.cap.set(cv.CAP_PROP_POS_FRAMES, frameNumber)
        self.frame = int(self.cap.get(cv.CAP_PROP_POS_FRAMES))

    def CreateOutputVideo(self, path):

        # Codec format to save video in
        # codec = cv.VideoWriter_fourcc(*'H264')
        codec = cv.VideoWriter_fourcc(*'avc1')

        # outPath = self.videoPath.split('.')
        self.out = cv.VideoWriter(path, codec, self.fps, self.frameSize)

    def SaveFrame(self, image):
        self.out.write(image)

    def Finish(self):
        self.out.release()
        # self.cap.release()

    def Close(self):
        self.cap.release()
        self.isOpen = False



""" ------------ Detect Video ------------ """

def DetectVideo(vid, boxes, func = None, output = '', save = False, frameStart = 0, frameEnd = 0, animal = 2, threshold = 0.5, convert = False, stopFlag = [0]):

    vid.SetFrame(frameStart)

    playFlag = vid.playFlag



    if save:
        vid.CreateOutputVideo(output)

    count = 0
    total = int(vid.frameCount)

    while playFlag:

        frame = vid.GetNextFrame()

        if np.any(frame) and vid.frame <= (frameEnd+1):
            playFlag = vid.playFlag

            print('\nProgress %s of %s' % (count+1, frameEnd - frameStart + 1))

            bBox, time = InferImage(frame, vid.frame)

            boxes[vid.frame] = bBox

            frameNew, animalBox, countBox = OverlayFrame(frame, bBox, animal, threshold, convert)

            print(countBox)
            print(animalBox)

            if save:
                vid.SaveFrame(frameNew)

            if func is not None:
                func(count+1, time)

            
            # cv.imshow('Video: Contours', frameNew)
        else:
            playFlag = False

        count += 1

        if stopFlag[0] == 1:
            playFlag = False

    if save:
        vid.Finish()


		
""" Intersection of bboxes """
def get_iou(b1, b2):
	bb1 = b1.astype(int)
	bb2 = b2.astype(int)

    # determine the coordinates of the intersection rectangle
	x_left = max(bb1[0], bb2[0])
	y_top = max(bb1[1], bb2[1])
	x_right = min(bb1[2], bb2[2])
	y_bottom = min(bb1[3], bb2[3])

	assert bb1[0] < bb1[2]
	assert bb1[1] < bb1[3]
	assert bb2[0] < bb2[2]
	assert bb2[1] < bb2[3]

	if x_right < x_left or y_bottom < y_top:
		return 0.0

    # The intersection of two axis-aligned bounding boxes is always an
    # axis-aligned bounding box
	intersection_area = (x_right - x_left) * (y_bottom - y_top)

    # compute the area of both AABBs
	bb1_area = (bb1[2] - bb1[0]) * (bb1[3] - bb1[1])
	bb2_area = (bb2[2] - bb2[0]) * (bb2[3] - bb2[1])

    # compute the intersection over union by taking the intersection
    # area and dividing it by the sum of prediction + ground-truth
    # areas - the interesection area
	iou = intersection_area / float(bb1_area + bb2_area - intersection_area)
	assert iou >= 0.0
	assert iou <= 1.0
	return iou
		
		
		
""" ------------ Overlay Frame ------------ """

def OverlayFrame(frame, bBox, animal = 2, threshold = 0.5, convert = False, layName = True, layConfidence = True, laySize = False, layCount = False):

    count = 0

    animalArray = [0, 0]

    # Size calculation
    focalLength = 24
    sensorWidth = 32
    widthRes = frame.shape[1]
    agl = 25
    gsd = ( (sensorWidth * agl) / (focalLength * widthRes) ) * 100

    size = "small"
    countBox = ""
    animalBox = ""
    # print(animal)
	
	
    # visualize detections
    for i in range(len(bBox.box[0])):
        box = bBox.box[0][i]
        score = bBox.score[0][i]
        label = bBox.label[0][i]
	
		# scores are sorted so we can break
        if score < threshold:
            break

		# remove double-up bboxes
        skipCurrent = False
        for j in range(i+1, len(bBox.box[0])):
            score2 = bBox.score[0][j]
            if score2 < threshold:
                break # skip scores that are too low

            box2 = bBox.box[0][j]
			
            if get_iou(box, box2) > 0.65:
                if animal == 2 or label2 != animal:
                    bBox.score[0][j] = 0.0  # set least confident score to 0 to skip
                else:
                    skipCurrent = True
		
        if skipCurrent:
            continue
		
        count += 1
        drawBox = True
        
        if (label != animal and animal != 2 and not convert):
            drawBox = False
        elif (label != animal and animal != 2 and convert):
            label = animal



        animalArray[label] +=1

        if drawBox:

            color = label_color(label)
            
            b = box.astype(int)

            lp = math.floor(math.sqrt(math.pow(b[3]-b[1], 2) + math.pow(b[2]-b[0], 2)))
            la = gsd * lp

            if label == 1:
                if la < 250:
                    size = 'small'
                elif la < 350:
                    size = 'medium'
                else:
                    size = 'large'
            elif label == 0:
                if la < 250:
                    size = 'small'
                elif la < 350:
                    size = 'medium'
                else:
                    size = 'large'

            # print(la)

            draw_box(frame, b, color = color, thickness = 1)
            
            caption = "{} ".format(count)

            if laySize:
                caption = caption + "{} ".format(size)

            if layName:
                caption = caption + "{} ".format(labels_to_names[label])

            if layConfidence:
                caption = caption + "{:.3f} ".format(score)

            draw_caption(frame, b, caption)

            animalBox = animalBox + "{}. {} {} {:.3f}\n".format(count, size, labels_to_names[label], score)


    # print(animalBox)


    if layCount:
        if animal == 0:
            cv.putText(frame, "Total Number of Whales: {}".format(animalArray[0]), (10, 30), cv.FONT_HERSHEY_SIMPLEX, 0.6, (0, 0, 255), 1, cv.LINE_AA)
        elif animal == 1:
            cv.putText(frame, "Total Number of Dolphins: {}".format(animalArray[1]), (10, 30), cv.FONT_HERSHEY_SIMPLEX, 0.6, (0, 0, 255), 1, cv.LINE_AA)
        elif animal == 2:
            cv.putText(frame, "Total Number of Whales: {}, Dolphins: {}".format(animalArray[0], animalArray[1]), (10, 30), cv.FONT_HERSHEY_SIMPLEX, 0.6, (0, 0, 255), 1, cv.LINE_AA)

    if animal == 0:
        countBox = "Total Whales: {}\n".format(animalArray[0])
    elif animal == 1:
        countBox = "Total Dolphins: {}\n".format(animalArray[1])
    elif animal == 2:
        countBox = "Total Whales: {}\nTotal Dolphins: {}\n".format(animalArray[0], animalArray[1])

    return frame, animalBox, countBox



# def AnimalSize(size, bBox, animal = 2, threshold = 0.5, convert = False):


    







def test(args):
    pass
    # LoadModel('resnet50_coco_best_v2.1.0.h5')
    # LoadModel('resnet50_best.h5')
    # image = read_image_bgr('000000008021.jpg')
    # InferImage(image)

    # vid = VideoControl('C:\\Users\\Jesse Haviland\\OneDrive\\University\\Year 4\\EGH455\data\\sample_dolphins_3.mp4')
    # vid.PrintVideoStats()
    # DetectVideo(vid)


    # vid.SetFrame(1000)
    # frame = vid.GetNextFrame()
    # cv.imshow('Video: Contours', frame)

    # vid.CreateOutputVideo()

    # vid.SaveFrame(frame)
    # frame = vid.GetNextFrame()
    # vid.SaveFrame(frame)
    # frame = vid.GetNextFrame()
    # vid.SaveFrame(frame)
    # frame = vid.GetNextFrame()

    # vid.Finish()

    # if cv.waitKey(5000) & 0xFF == ord('q'):
    #     pass
        # time.sleep(5)

    

    



if __name__=='__main__':
    params = args()
    test(params)